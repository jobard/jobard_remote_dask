# syntax=docker/dockerfile:experimental

# ===========================================================
# RUNTIME BASE VENV
# ===========================================================
FROM gitlab-registry.ifremer.fr/ifremer-commons/docker/internal-images/mamba-poetry:mamba-poetry-1.3.1 as conda-base

ARG CONDA_ENV_PATH="/venv/"
ENV CONDA_ENV_PATH=$CONDA_ENV_PATH

WORKDIR /tmp

# install runtime conda dependencies
COPY assets/conda/conda-run-linux-64.lock ./
RUN --mount=type=cache,target=/opt/conda/pkgs \
    mamba create --copy -y -p $CONDA_ENV_PATH --file conda-run-linux-64.lock

# install runtime python dependencies (need a .git to use poetry-dynamic-versioning)
COPY poetry.lock pyproject.toml ./
RUN git init \
    && poetry lock --no-update \
	&& conda run -p $CONDA_ENV_PATH poetry install --only main

# ===========================================================
# DEV BASE VENV
# ===========================================================
FROM conda-base as development

# install dev conda dependencies
COPY assets/conda/conda-dev-linux-64.lock ./
RUN --mount=type=cache,target=/opt/conda/pkgs \
    mamba create --copy -y -p $CONDA_ENV_PATH --file conda-dev-linux-64.lock

# install dev python dependencies
RUN conda run -p $CONDA_ENV_PATH poetry install --with dev --no-root

# ============================================================
# RUNTIME FINAL VENV
# ============================================================
FROM conda-base as base-runtime

# build and install project
COPY . .
RUN conda run -p $CONDA_ENV_PATH poetry build --format wheel \
    && conda run -p $CONDA_ENV_PATH pip install dist/*.whl --no-deps

# cleanup conda environment
RUN find $CONDA_ENV_PATH -name '*.a' -delete && \
    rm -rf $CONDA_ENV_PATH/conda-meta && \
    rm -rf $CONDA_ENV_PATH/include && \
    find $CONDA_ENV_PATH -name '__pycache__' -type d -exec rm -rf '{}' '+' && \
    rm -rf $CONDA_ENV_PATH/lib/python*/site-packages/pip  \
        $CONDA_ENV_PATH/lib/python*/idlelib  \
        $CONDA_ENV_PATH/lib/python*/ensurepip \
        $CONDA_ENV_PATH/bin/x86_64-conda-linux-gnu-ld \
        $CONDA_ENV_PATH/bin/openssl \
        $CONDA_ENV_PATH/share/terminfo && \
    find $CONDA_ENV_PATH/lib/python*/site-packages -maxdepth 1  -mindepth 1  -name 'tests' -type d -exec rm -rf '{}' '+' && \
    find $CONDA_ENV_PATH/lib/python*/site-packages -name '*.pyx' -delete

FROM debian:buster-slim as runtime

ARG CONDA_ENV_PATH="/venv"
COPY --from=base-runtime $CONDA_ENV_PATH $CONDA_ENV_PATH
ENV PATH=$CONDA_ENV_PATH/bin:$PATH \
    PYTHONWARNINGS="ignore"

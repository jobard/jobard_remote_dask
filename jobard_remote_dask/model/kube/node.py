"""Kubernetes node information class."""
from jobard_remote_dask.model.base import JobardModel


class NodeInfos(JobardModel):
    """Node information class."""

    hostname: str                                     # Kubernetes node hostname
    cpu_declared: int                                 # Declared CPU Nanos
    mem_declared: int                                 # Declared MEM in bytes

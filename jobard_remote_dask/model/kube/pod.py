"""Kubernetes pod information class."""
from jobard_remote_dask.model.base import JobardModel


class PodInfos(JobardModel):
    """Pod information class."""

    id: str                                           # Kubernetes Pod ID
    is_jobard_unit: bool                              # Whenever it is a "jobarded" Docker image
    cpu_declared: int                                 # Declared CPU Nanos
    mem_declared: int                                 # Declared MEM in bytes

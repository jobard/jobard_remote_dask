"""Host class."""
from jobard_remote_dask.model.base import JobardModel


class Host(JobardModel):
    """Host class."""

    hostname: str                                     # Swarm node hostname
    cpu_declared: int                                 # Declared CPU Nanos
    mem_declared: int                                 # Declared MEM in bytes

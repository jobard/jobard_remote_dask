"""Container class."""
from jobard_remote_dask.model.base import JobardModel


class Container(JobardModel):
    """Container class."""

    id: str                                           # Container ID
    is_jobard_unit: bool                              # Whenever it is a "jobarded" Docker image
    cpu_declared: int                                 # Declared CPU Nanos
    mem_declared: int                                 # Declared MEM in bytes

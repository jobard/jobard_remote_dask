"""Jobard Base Model."""
from pydantic import BaseModel


class JobardModel(BaseModel):
    """Abstract model class."""

    class Config:  # noqa: WPS306,WPS431
        """Pydantic configuration."""

        validate_all = True
        validate_assignment = True

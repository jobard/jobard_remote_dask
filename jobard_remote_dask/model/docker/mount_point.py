"""Container class."""
from pathlib import Path

from jobard_remote_dask.model.base import JobardModel


class MountPoint(JobardModel):
    """MountPoint class."""

    name: str                               # Mount point name
    source: Path                            # Mount source directory on container
    destination: Path                       # Mount destination directory on host

"""AppEvent class that describe a distributed application to trigger."""
from pathlib import Path
from typing import Optional

from pydantic import PositiveInt, NonNegativeInt

from jobard_remote_dask.core.type.app import AppType
from jobard_remote_dask.core.type.basic import Arguments, Memory, Walltime, MountPoints, CommandAlias, JobOrderName
from jobard_remote_dask.core.type.cluster import Cluster
from jobard_remote_dask.model.base import JobardModel


class AppEvent(JobardModel):
    """AppEvent class that describe a distributed application to trigger."""

    # Application General Information
    job_order_id: PositiveInt                           # job order id
    job_array_id: PositiveInt                           # job array id
    try_id: PositiveInt = 1                             # try id
    job_order_name: Optional[JobOrderName]              # job order name
    command_alias: CommandAlias                         # command alias
    name: str = 'JobardApp'                             # app name
    app_type: AppType = AppType.DASK                    # app type
    cluster_type: Cluster = Cluster.LOCAL               # cluster type

    # Application Driver Spec (in case of Dask, the driver refers to the scheduler)
    driver_cores: PositiveInt                           # cores allocated for the app driver
    driver_memory: Memory                               # memory to allocate for the driver

    # Application Worker Spec
    worker_disk: Memory = '1M'                          # disk to allocate for a given worker
    worker_cores: PositiveInt                           # cores to allocate for a given worker
    worker_memory: Memory                               # memory to allocate for a given worker
    n_workers: PositiveInt                              # specify number of workers explicitly
    n_min_workers: PositiveInt = None                   # or dynamically scale based workers
    n_max_workers: PositiveInt = None                   # or dynamically scale based workers

    # Log dir
    log_chroot: Path                                    # base for logging

    # Other Spec
    queue: str = None                                   # queue
    walltime: Walltime = '00:30:00'                     # walltime
    image: str = None                                   # docker image to load
    unix_uid: NonNegativeInt = None                     # unix uid for docker images (SWARM) or pods (KUBE)
    docker_mount_points: MountPoints = None             # mount points for docker image (name, source, destination)

    # Timeout
    application_processor_timeout: PositiveInt = 240    # jobard application processor timeout (plus walltime)
    system_limit_timeout: PositiveInt = 120             # system limit timeout (plus walltime)
    death_timeout: PositiveInt = 60                     # auto kill if app is inactive within death_timeout seconds

    # Extra
    driver_extra_args: Arguments = None                 # extra args for the underlying driver/scheduler
    worker_extra_args: Arguments = None                 # extra args for the underlying worker
    jobard_extra_args: Arguments = None                 # extra args related to jobard (eg ['per_job_timeout=60'])

"""ResponseHeaderEvent class."""
from pydantic import NonNegativeInt

from jobard_remote_dask.model.base import JobardModel


class ResponseHeaderEvent(JobardModel):
    """ResponseHeaderEvent class."""

    obj_count: NonNegativeInt                           # number of objects into the response body

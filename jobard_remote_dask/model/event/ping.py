"""PingEvent class."""
from jobard_remote_dask.model.base import JobardModel


class PingEvent(JobardModel):
    """PingEvent class."""

    message: str                                           # message to transmit

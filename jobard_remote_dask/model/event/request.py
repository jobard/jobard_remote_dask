"""RequestHeaderEvent class."""
from pydantic import NonNegativeInt

from jobard_remote_dask.core.type.op import Op
from jobard_remote_dask.model.base import JobardModel


class RequestHeaderEvent(JobardModel):
    """RequestHeaderEvent class."""

    op: Op                                           # request header op
    obj_count: NonNegativeInt                           # number of objects into the request body

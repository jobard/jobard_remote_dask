"""JobEvent class that describe a job sent to the remote executors."""
from typing_extensions import Optional

from jobard_remote_dask.core.type.basic import Arguments
from jobard_remote_dask.model.base import JobardModel


class JobEvent(JobardModel):
    """JobEvent class that describe a job sent to the remote executors."""

    id: int                                           # job id
    command: Arguments                                # command to execute
    job_prefix: Optional[str]                         # job log file prefix

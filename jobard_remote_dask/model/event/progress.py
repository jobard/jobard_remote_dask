"""JobProgressEvent class that describe a progress for a particular job."""
from typing import Optional

from pydantic import confloat

from jobard_remote_dask.core.type.state import State
from jobard_remote_dask.model.base import JobardModel


class JobProgressEvent(JobardModel):
    """JobProgressEvent class that describe a progress for a particular job."""

    id: int                                           # job id
    progress: confloat(ge=0, le=1) = 0                # progress
    state: State = State.NEW                          # state
    job_prefix: Optional[str]                         # job log file prefix


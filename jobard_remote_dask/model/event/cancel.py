"""JobCancelEvent class."""

from jobard_remote_dask.model.base import JobardModel


class JobCancelEvent(JobardModel):
    """JobCancelEvent class."""

    id: int                                           # job id

    def __hash__(self):
        return self.id

    def __eq__(self, other):
        return other.id == self.id

"""Simple Jobard Tcp Protocol, that avoid pickle for security reasons."""
import json
import pathlib

from jobard_remote_dask.model.event.app import AppEvent
from jobard_remote_dask.model.event.cancel import JobCancelEvent
from jobard_remote_dask.model.event.job import JobEvent
from jobard_remote_dask.model.event.ping import PingEvent
from jobard_remote_dask.model.event.progress import JobProgressEvent
from jobard_remote_dask.model.event.request import RequestHeaderEvent
from jobard_remote_dask.model.event.response import ResponseHeaderEvent


class JProtocol:
    """Simple Jobard Tcp Protocol"""

    @staticmethod
    def encode_request_header(obj: RequestHeaderEvent) -> str:
        """Build the request header"""
        return json.dumps({'op': obj.op, 'obj_count': obj.obj_count})

    @staticmethod
    def decode_request_header(payload: str) -> RequestHeaderEvent:
        """Build the RequestHeaderEvent object"""
        obj = json.loads(payload)
        return RequestHeaderEvent(op=obj['op'], obj_count=obj['obj_count'])

    @staticmethod
    def encode_response_header(obj: ResponseHeaderEvent) -> str:
        """Build the response header"""
        return json.dumps({'obj_count': obj.obj_count})

    @staticmethod
    def decode_response_header(payload: str) -> ResponseHeaderEvent:
        """Build the ResponseHeaderEvent object"""
        obj = json.loads(payload)
        return ResponseHeaderEvent(obj_count=obj['obj_count'])

    @staticmethod
    def encode_job_event(obj: JobEvent) -> str:
        """Encode a JobEvent object"""
        return json.dumps({
            'id': obj.id,
            'command': obj.command,
            'job_prefix': obj.job_prefix,
        })

    @staticmethod
    def decode_job_event(payload: str) -> JobEvent:
        """Decode a JobEvent object"""
        obj = json.loads(payload)
        return JobEvent(id=obj['id'], command=obj['command'], job_prefix=obj['job_prefix'])

    @staticmethod
    def encode_job_progress_event(obj: JobProgressEvent) -> str:
        """Encode a JobProgressEvent object"""
        return json.dumps({
            'id': obj.id,
            'progress': obj.progress,
            'state': obj.state,
            'job_prefix': obj.job_prefix,
        })

    @staticmethod
    def decode_job_progress_event(payload: str) -> JobProgressEvent:
        """Decode a JobProgressEvent object"""
        obj = json.loads(payload)
        return JobProgressEvent(id=obj['id'], progress=obj['progress'], state=obj['state'], job_prefix=obj['job_prefix'])

    @staticmethod
    def encode_ping_event(obj: PingEvent) -> str:
        """Encode a PingEvent object"""
        return json.dumps({
            'message': obj.message,
        })

    @staticmethod
    def decode_ping_event(payload: str) -> PingEvent:
        """Decode a PingEvent object"""
        obj = json.loads(payload)
        return PingEvent(message=obj['message'])

    @staticmethod
    def encode_job_cancel_event(obj: JobCancelEvent) -> str:
        """Encode a JobCancelEvent object"""
        return json.dumps({
            'id': obj.id,
        })

    @staticmethod
    def decode_job_cancel_event(payload: str) -> JobCancelEvent:
        """Decode a JobCancelEvent object"""
        obj = json.loads(payload)
        return JobCancelEvent(id=obj['id'])

    @staticmethod
    def encode_app_event(obj: AppEvent) -> str:
        """Encode an AppEvent object"""
        return json.dumps({
            'job_order_id': obj.job_order_id,
            'job_array_id': obj.job_array_id,
            'try_id': obj.try_id,
            'job_order_name': obj.job_order_name,
            'command_alias': obj.command_alias,
            'name': obj.name,
            'app_type': obj.app_type,
            'cluster_type': obj.cluster_type,

            'driver_cores': obj.driver_cores,
            'driver_memory': obj.driver_memory,

            'worker_disk': obj.worker_disk,
            'worker_cores': obj.worker_cores,
            'worker_memory': obj.worker_memory,
            'n_workers': obj.n_workers,
            'n_min_workers': obj.n_min_workers,
            'n_max_workers': obj.n_max_workers,

            'log_chroot': obj.log_chroot.as_posix(),

            'queue': obj.queue,
            'walltime': obj.walltime,
            'image': obj.image,
            'docker_mount_points': obj.docker_mount_points,

            'application_processor_timeout': obj.application_processor_timeout,
            'system_limit_timeout': obj.system_limit_timeout,
            'death_timeout': obj.death_timeout,

            'driver_extra_args': obj.driver_extra_args,
            'worker_extra_args': obj.worker_extra_args,
            'jobard_extra_args': obj.jobard_extra_args,

            'unix_uid': obj.unix_uid,
        })

    @staticmethod
    def decode_app_event(payload: str) -> AppEvent:
        """Decode an AppEvent object"""
        obj = json.loads(payload)
        return AppEvent(
            job_order_id=obj['job_order_id'],
            job_array_id=obj['job_array_id'],
            try_id=obj['try_id'],
            job_order_name=obj['job_order_name'],
            command_alias=obj['command_alias'],
            name=obj['name'],
            app_type=obj['app_type'],
            cluster_type=obj['cluster_type'],

            driver_cores=obj['driver_cores'],
            driver_memory=obj['driver_memory'],

            worker_disk=obj['worker_disk'],
            worker_cores=obj['worker_cores'],
            worker_memory=obj['worker_memory'],
            n_workers=obj['n_workers'],
            n_min_workers=obj['n_min_workers'],
            n_max_workers=obj['n_max_workers'],

            log_chroot=pathlib.Path(obj['log_chroot']),

            queue=obj['queue'],
            walltime=obj['walltime'],
            image=obj['image'],
            docker_mount_points=obj['docker_mount_points'],

            application_processor_timeout=obj['application_processor_timeout'],
            system_limit_timeout=obj['system_limit_timeout'],
            death_timeout=obj['death_timeout'],

            driver_extra_args=obj['driver_extra_args'],
            worker_extra_args=obj['worker_extra_args'],
            jobard_extra_args=obj['jobard_extra_args'],

            unix_uid=obj['unix_uid'],
        )

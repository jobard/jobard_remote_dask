"""Jobard Tcp Client class"""
import asyncio
from typing import List

from jobard_remote_dask.core.exceptions import JobardError
from jobard_remote_dask.core.type.op import Op
from jobard_remote_dask.model.event.app import AppEvent
from jobard_remote_dask.model.event.cancel import JobCancelEvent
from jobard_remote_dask.model.event.job import JobEvent
from jobard_remote_dask.model.event.ping import PingEvent
from jobard_remote_dask.model.event.progress import JobProgressEvent
from jobard_remote_dask.model.event.request import RequestHeaderEvent
from jobard_remote_dask.tcp.base import TCPBase
from jobard_remote_dask.tcp.protocol import JProtocol


class TCPClient(TCPBase):
    """Jobard Tcp Client class"""

    def __init__(
            self,
            timeout: int,
            connect_timeout: int,
            host: str,
            port: int,
            wait_event=None,
            end_event=None,
            loop=None,
    ):
        """Configure the TCP server"""

        self.server = None
        self.host = host
        self.port = port
        self.wait_event = wait_event
        self.end_event = end_event
        self.loop = loop

        super().__init__(timeout, connect_timeout)

    def set_loop(self, loop):
        self.loop = loop

    async def open(self):
        try:
            return await asyncio.wait_for(
                asyncio.open_connection(host=self.host, port=self.port, loop=self.loop),
                timeout=self.connect_timeout
            )
        except (asyncio.TimeoutError, asyncio.CancelledError):
            raise JobardError('ping : a timeout occurred while connecting')

    async def wait(self):
        if self.wait_event is not None:
            await self.wait_event.wait()

    def end(self):
        if self.end_event is not None:
            self.end_event.set()

    async def write_request_header(self, writer, op: Op, obj_count: int, extra_infos):
        request_header = JProtocol.encode_request_header(RequestHeaderEvent(op=op, obj_count=obj_count))
        await self.write_object(writer=writer, data=request_header, extra_infos=extra_infos)

    async def read_response_header(self, reader, extra_infos):
        response_header = await self.read_object(reader=reader, extra_infos=extra_infos)
        return JProtocol.decode_response_header(response_header)

    async def ping(self) -> PingEvent:
        """Do a ping operation"""

        writer = None
        try:
            # open connection
            await self.wait()
            reader, writer = await self.open()

            # extra infos
            extra_infos = self.tcp_extra(writer)

            # write request header
            await self.write_request_header(writer=writer, op=Op.PING, obj_count=0, extra_infos=extra_infos)

            # read response header
            await self.read_response_header(reader=reader, extra_infos=extra_infos)

            # read response body
            response_body = await self.read_object(reader=reader, extra_infos=extra_infos)
            pong = JProtocol.decode_ping_event(response_body)

        finally:
            await self.close_writer(writer)

        self.end()

        return pong

    async def get_app(self) -> AppEvent:
        """Do a GetApp operation"""

        writer = None
        try:
            # open connection
            await self.wait()
            reader, writer = await self.open()

            # extra infos
            extra_infos = self.tcp_extra(writer)

            # write request header
            await self.write_request_header(writer, Op.GET_APP, 0, extra_infos)

            # read response header
            await self.read_response_header(reader=reader, extra_infos=extra_infos)

            # read response body
            response_body = await self.read_object(reader=reader, extra_infos=extra_infos)
            app = JProtocol.decode_app_event(response_body)

        finally:
            await self.close_writer(writer)

        self.end()

        return app

    async def get_commands(self) -> List[JobEvent]:
        """Ask for commands operation"""

        ret: List[JobEvent] = []
        writer = None
        try:
            # open connection
            await self.wait()
            reader, writer = await self.open()

            # extra infos
            extra_infos = self.tcp_extra(writer)

            # write request header
            await self.write_request_header(writer, Op.GET_COMMANDS, 0, extra_infos)

            # read response header
            response_header = await self.read_response_header(reader=reader, extra_infos=extra_infos)

            # read response body
            for i in range(0, response_header.obj_count):
                response_body = await self.read_object(reader=reader, extra_infos=extra_infos)
                ret.append(JProtocol.decode_job_event(response_body))

        finally:
            await self.close_writer(writer)

        self.end()

        return ret

    async def get_jobs_to_cancel(self) -> List[JobCancelEvent]:
        """Ask for jobs to cancel operation"""

        ret: List[JobCancelEvent] = []
        writer = None
        try:
            # open connection
            await self.wait()
            reader, writer = await self.open()

            # extra infos
            extra_infos = self.tcp_extra(writer)

            # write request header
            await self.write_request_header(writer, Op.JOB_CANCEL, 0, extra_infos)

            # read response header
            response_header = await self.read_response_header(reader=reader, extra_infos=extra_infos)

            # read response body
            for i in range(0, response_header.obj_count):
                response_body = await self.read_object(reader=reader, extra_infos=extra_infos)
                ret.append(JProtocol.decode_job_cancel_event(response_body))

        finally:
            await self.close_writer(writer)

        self.end()

        return ret

    async def report_progress(self, jobs: List[JobProgressEvent]) -> None:
        """Reporting job progress, back to the daemon"""

        jobs_len = len(jobs)
        writer = None
        try:
            # open connection
            await self.wait()
            reader, writer = await self.open()

            # extra infos
            extra_infos = self.tcp_extra(writer)

            # write request header
            await self.write_request_header(writer, Op.JOB_REPORT_PROGRESS, jobs_len, extra_infos)

            for i in range(0, jobs_len):
                await self.write_object(
                    writer=writer,
                    data=JProtocol.encode_job_progress_event(jobs[i]),
                    extra_infos=extra_infos
                )

            # read response header
            await self.read_response_header(reader=reader, extra_infos=extra_infos)

        finally:
            await self.close_writer(writer)

        self.end()

"""Jobard Tcp Base class."""
import asyncio
from typing import Dict

from jobard_remote_dask.core.exceptions import JobardError


class TCPBase:
    """Jobard Tcp Base class."""

    ENCODING = r'utf8'

    def __init__(self, timeout: int, connect_timeout: int):
        self.timeout = timeout
        self.connect_timeout = connect_timeout
        self._logger = None

    def set_logger(self, logger):
        self._logger = logger

    @staticmethod
    def tcp_extra(writer) -> Dict[str, str]:
        return {'peer_name': writer.get_extra_info('peername')}

    @staticmethod
    def flatten_peer_name(peer_name):
        return peer_name[0] + ':' + str(peer_name[1])

    async def read_object(self, reader, extra_infos) -> str:
        flatten_peer_name = self.flatten_peer_name(extra_infos['peer_name'])
        try:
            bytes_len = await asyncio.wait_for(reader.readline(), timeout=self.timeout)
            msg_len = int(bytes_len)
            all_data = await asyncio.wait_for(reader.readexactly(msg_len), timeout=self.timeout)
        except ConnectionRefusedError:
            raise JobardError('read_object : connection refused from {0}'.format(flatten_peer_name))
        except ConnectionResetError:
            raise JobardError('read_object : connection reset from {0}'.format(flatten_peer_name))
        except ConnectionAbortedError:
            raise JobardError('read_object : server timed out connection from {0}'.format(flatten_peer_name))
        except ConnectionError:
            raise JobardError('read_object : connection error from {0}'.format(flatten_peer_name))
        except (asyncio.TimeoutError, asyncio.CancelledError):
            raise JobardError(
                'read_object : did not get answer from server due to timeout from '.format(flatten_peer_name),
            )
        return all_data.decode(self.ENCODING)

    async def write_object(self, writer, data: str, extra_infos) -> None:
        flatten_peer_name = self.flatten_peer_name(extra_infos['peer_name'])
        try:
            data_encoded = data.encode(self.ENCODING)
            writer.write(b'%d\n' % len(data_encoded))
            writer.write(data_encoded)
            await asyncio.wait_for(writer.drain(), timeout=self.timeout)
        except ConnectionRefusedError:
            raise JobardError('write_object : connection refused to {0}'.format(flatten_peer_name))
        except ConnectionResetError:
            raise JobardError('write_object : connection reset to {0}'.format(flatten_peer_name))
        except ConnectionAbortedError:
            raise JobardError(
                'write_object : server timed out connection to {0}'.format(flatten_peer_name),
            )
        except ConnectionError:
            raise JobardError('write_object : connection error to {0}'.format(flatten_peer_name))
        except (asyncio.TimeoutError, asyncio.CancelledError):
            raise JobardError(
                'write_object : did not get answer from server due to timeout to {0}'.format(flatten_peer_name),
            )

    async def close_writer(self, writer):
        try:
            if writer is not None:
                if not writer.is_closing():
                    writer.close()
                    await asyncio.wait_for(writer.wait_closed(), timeout=self.timeout)
        except Exception as be:
            self._logger.debug(str(be))

"""Jobard Tcp Client class"""
import asyncio
from typing import List

from jobard_remote_dask.core.exceptions import JobardError
from jobard_remote_dask.model.event.app import AppEvent
from jobard_remote_dask.model.event.cancel import JobCancelEvent
from jobard_remote_dask.model.event.job import JobEvent
from jobard_remote_dask.model.event.ping import PingEvent
from jobard_remote_dask.model.event.progress import JobProgressEvent


class FakeTCPClient:
    """Jobard Tcp Client class"""

    def __init__(
            self,
            app: AppEvent,
            jobs: List[JobEvent],
            simulate_ping_exception: bool,
            simulate_cancellation_asked: bool,
            wait_event=None,
            end_event=None,
            loop=None,
    ):
        """Configure the TCP server"""

        self.app = app
        self.jobs = jobs

        self.simulate_cancellation_asked = simulate_cancellation_asked
        self.simulate_ping_exception = simulate_ping_exception

        self.wait_event = wait_event
        self.end_event = end_event
        self.loop = loop
        self._logger = None

    def set_logger(self, logger):
        self._logger = logger

    def set_loop(self, loop):
        self.loop = loop

    async def wait(self):
        if self.wait_event is not None:
            await self.wait_event.wait()

    def end(self):
        if self.end_event is not None:
            self.end_event.set()

    async def ping(self) -> PingEvent:
        """Do a ping operation"""

        await asyncio.sleep(1)

        self.end()

        if self.simulate_ping_exception:
            raise JobardError('ping exception')

        return PingEvent(message='pong')

    async def get_app(self) -> AppEvent:
        """Do a GetApp operation"""

        await asyncio.sleep(1)

        self.end()

        return self.app

    async def get_commands(self) -> List[JobEvent]:
        """Ask for commands operation"""

        await asyncio.sleep(1)

        self.end()

        return self.jobs

    async def get_jobs_to_cancel(self) -> List[JobCancelEvent]:
        """Ask for jobs to cancel operation"""

        ret: List[JobCancelEvent] = []

        await asyncio.sleep(1)

        if self.simulate_cancellation_asked:
            ret = [JobCancelEvent(id=1)]

        return ret

    async def report_progress(self, jobs: List[JobProgressEvent]) -> None:
        """Reporting job progress, back to the daemon"""

        await asyncio.sleep(1)

        self.end()

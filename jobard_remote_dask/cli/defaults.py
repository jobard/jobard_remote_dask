import pathlib

JOBARD_DASK_CONFIG = pathlib.Path('~/.config/jobard/dask/')

TCP_TIMEOUT = 10
TCP_CONNECT_TIMEOUT = 20

CLOSE_TIMEOUT = 10

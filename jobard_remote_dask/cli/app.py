import asyncio
import logging
import sys

import click

from jobard_remote_dask.cli import defaults
from jobard_remote_dask.core.app import DaskUnitJobApp, dask_app_handle_exception
from jobard_remote_dask.core.logger.log import JobardLogger
from jobard_remote_dask.tcp.client import TCPClient


def run_app(
        log_level,
        internal_tcp_client,
        terminate_on_task_error: bool = False,
        enforce_resources_constraints: bool = False
):
    """
    Create the dask remote object and start it.

    Args:
        log_level: The logging level.
        internal_tcp_client: The TCP client used for exchange between the jobard daemon and the remote dask unit.
        terminate_on_task_error: If set to True: terminate the whole Jobard app if at least one task is in failed state.
        enforce_resources_constraints: If set to True: add CPU and memory constraints for the subprocess at the unix
        level.
    """

    # logging
    logger = JobardLogger(log_level)
    logger.create()

    # get the loop
    loop = asyncio.new_event_loop()
    loop.set_debug(False)
    asyncio.set_event_loop(loop)

    # set the loop for the tcp client
    internal_tcp_client.set_loop(loop)

    # set the logger
    internal_tcp_client.set_logger(logger)

    # create the dask remote object
    dask_unit_job_app = DaskUnitJobApp(
        dask_config_file=defaults.JOBARD_DASK_CONFIG,
        tcp_client=internal_tcp_client,
        close_timeout=defaults.CLOSE_TIMEOUT,
        loop=loop,
        logger=logger,
        terminate_on_task_error=terminate_on_task_error,
        enforce_resources_constraints=enforce_resources_constraints,
    )

    # set asyncio un-catch exception error handler
    loop.set_exception_handler(dask_app_handle_exception(dask_unit_job_app, logger))

    try:
        # do process
        loop.run_until_complete(dask_unit_job_app.run())
    finally:
        try:
            loop.run_until_complete(dask_unit_job_app.cleanup())
        finally:
            try:
                loop.stop()
            finally:
                try:
                    loop.close()
                finally:
                    logger.close()


@click.command('jobard_remote_dask')
@click.argument('host', type=str)
@click.argument('port', type=int)
def jobard_remote_dask(host: str, port: int):
    """
    Jobard remote dask entry point :
    - Create a TCP Client for exchange with jobard daemon.
    - Start the remote dask unit.

    Args:
        host : hostname of the TCP server.
        port : port number.
    """

    # create the tcp client
    tcp_client = TCPClient(
        timeout=defaults.TCP_TIMEOUT,
        connect_timeout=defaults.TCP_CONNECT_TIMEOUT,
        host=host,
        port=port
    )

    # run the app
    run_app(logging.INFO, tcp_client)

    sys.exit(0)

import logging
import pathlib
import random
import sys
from typing import List

from jobard_remote_dask.cli.app import run_app
from jobard_remote_dask.core.type.app import AppType
from jobard_remote_dask.core.type.cluster import Cluster
from jobard_remote_dask.model.event.app import AppEvent
from jobard_remote_dask.model.event.job import JobEvent
from jobard_remote_dask.tcp.fakeclient import FakeTCPClient

if __name__ == '__main__':

    # app spec
    app = AppEvent(
        job_order_id=random.randint(1, sys.maxsize),
        job_array_id=1,
        try_id=1,
        command_alias='test_local',
        name='test_local',
        app_type=AppType.DASK_JOB_ARRAY,
        cluster_type=Cluster.LOCAL,

        walltime='00:10:00',

        driver_cores=1,
        driver_memory='2GB',

        worker_cores=1,
        worker_memory='2GB',
        n_workers=2,
        n_min_workers=None,
        n_max_workers=None,

        log_chroot=pathlib.Path(r'/tmp/jobard/remote_dask/log/'),

        application_processor_timeout=240,
        system_limit_timeout=120,
        death_timeout=60,
    )

    # job spec
    jobs: List[JobEvent] = [
            JobEvent(
                id=1,
                command=[
                    'echo',
                    'Hello',
                ]
            )
    ]

    # create the tcp client
    tcp_client = FakeTCPClient(
        simulate_ping_exception=False,
        simulate_cancellation_asked=False,
        app=app,
        jobs=jobs,
    )

    # run the app
    run_app(logging.DEBUG, tcp_client)

    sys.exit(0)

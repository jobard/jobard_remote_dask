"""Jobard logger."""
import logging
import sys


class JobardLogger:
    """Jobard logger."""

    def __init__(self, level):
        self.level = level
        self.logger = None
        self.handler = None

    def create(self):
        self.logger = logging.getLogger('jobard_remote_dask')
        self.logger.setLevel(self.level)
        self.handler = logging.StreamHandler(sys.stdout)
        self.handler.setLevel(self.level)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        self.handler.setFormatter(formatter)
        self.logger.addHandler(self.handler)

    def flush(self):
        sys.stdout.flush()
        self.handler.flush()

    def close(self):
        self.flush()
        logging.shutdown()

    def info(self, msg):
        self.logger.info(msg)
        self.flush()

    def debug(self, msg):
        self.logger.debug(msg)
        self.flush()

    def warn(self, msg):
        self.logger.warning(msg)
        self.flush()

    def critical(self, msg):
        self.logger.critical(msg)
        self.flush()

    def error(self, msg):
        self.logger.error(msg)
        self.flush()

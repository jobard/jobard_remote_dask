"""Generic exceptions."""


class JobardError(Exception):
    """Jobard generic error."""


class JobardClusterCriticalError(Exception):
    """Cluster critical error."""


class JobardTimeoutError(JobardError):
    """Jobard generic timeout error."""


class JobardConversionError(JobardError):
    """Jobaord conversion error."""

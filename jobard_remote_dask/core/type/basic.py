"""Basic types."""
from typing import List, Tuple, Dict

from pydantic import constr

Memory = constr(regex=r'\d+[GMK]')
Walltime = constr(regex=r'^\d{2}:\d{2}:\d{2}$')
Split = constr(regex=r'\/?[1-9]\d*')
Arguments = List[str]
MountPoints = Dict[str, Tuple[str, str]]

CommandAlias = constr(regex=r'^[A-Za-z0-9_-]*$')
JobOrderName = constr(regex=r'^[A-Za-z0-9_-]*$')
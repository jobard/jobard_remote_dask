"""Cluster type."""
from enum import Enum


class Cluster(str, Enum):
    """Cluster types."""

    LOCAL = 'LOCAL'
    SWARM = 'SWARM'
    PBS = 'PBS'
    HTCONDOR = 'HTCONDOR'
    KUBE = 'KUBE'


class ClusterDaskConfig(str, Enum):
    """Cluster Dask configuration."""

    LOCAL = 'jobqueue.yaml'
    SWARM = 'swarm.yaml'
    PBS = 'jobqueue.yaml'
    HTCONDOR = 'jobqueue.yaml'
    KUBE = 'kube.yaml'
"""State type."""
from enum import Enum


class State(str, Enum):
    """Execution states."""

    NEW = 'new'
    SUBMITTED = 'submitted'
    RUNNING = 'running'
    SUCCEEDED = 'succeeded'
    FAILED = 'failed'
    CANCELLATION_ASKED = 'cancellation_asked'
    CANCELLED = 'cancelled '

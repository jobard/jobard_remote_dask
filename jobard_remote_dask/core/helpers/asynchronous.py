import asyncio

from jobard_remote_dask.core.exceptions import JobardTimeoutError


async def async_popen(*args, timeout, custom_logger=None):
    """Async version of subprocess popen"""

    process = None
    try:

        # initialize subprocess
        process = await asyncio.wait_for(
            asyncio.create_subprocess_exec(
                *args,
                stdout=asyncio.subprocess.PIPE,
                stderr=asyncio.subprocess.PIPE
            ),
            timeout=timeout
        )

        # wait until subprocess end
        stdout, stderr = await asyncio.wait_for(
            process.communicate(),
            timeout=timeout
        )

        # decode strings
        return process.returncode, stdout.decode(), stderr.decode()

    except (asyncio.TimeoutError, asyncio.CancelledError):

        # trigger a proper error, in case of coroutines timeout
        raise JobardTimeoutError(
            'timeout while running command : ' + ' '.join(args)
        )

    finally:

        # anyway, terminate the subprocess before returning
        if process is not None:
            try:
                process.kill()
            except Exception as e:
                custom_logger.debug(e)

"""Human parsing module."""
from functools import lru_cache
from typing import Mapping

from jobard_remote_dask.core.exceptions import JobardConversionError


@lru_cache(maxsize=None)
def byte_sizes() -> Mapping[str, int]:
    """Generate available byte sizes mapping.

    Returns:
        Mapping <format>:<size>
    """
    byte_sizes_base = {
        'kB': 10 ** 3,
        'MB': 10 ** 6,
        'GB': 10 ** 9,
        'TB': 10 ** 12,
        'PB': 10 ** 15,
        'KiB': 2 ** 10,
        'MiB': 2 ** 20,
        'GiB': 2 ** 30,
        'TiB': 2 ** 40,
        'PiB': 2 ** 50,
        'B': 1,
        '': 1,
    }
    mapping = {key.lower(): value for key, value in byte_sizes_base.items()}
    mapping.update({key[0]: value for key, value in mapping.items() if key and 'i' not in key})
    mapping.update({key[:-1]: value for key, value in mapping.items() if key and 'i' in key})

    return mapping


def parse_human_size(string: str) -> int:
    """Parse byte string to number.

    Args:
        string: the input string

    Returns:
        int: corresponding number

    Raises:
        ConversionError: bad format
    """

    string = string.replace(' ', '')
    if not any(char.isdigit() for char in string):
        string = '1{0}'.format(string)

    index_alpha = 0
    for index_alpha in range(len(string) - 1, -1, -1):
        if not string[index_alpha].isalpha():
            break
    index_alpha += 1

    prefix = string[:index_alpha]
    suffix = string[index_alpha:]

    try:
        return int(float(prefix) * byte_sizes()[suffix.lower()])
    except (KeyError, ValueError) as error:
        raise JobardConversionError('Unable to convert human size : {0}'.format(str(error)))

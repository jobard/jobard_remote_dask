"""CPU parsing module."""


def to_nanocores(cpu: str) -> int:
    return int(cpu.replace('m', '')) * 1000000 if cpu.find('m') != -1 else int(cpu) * 1000000000

"""Arguments utils."""

ARGUMENTS_UNABLE_TO_DETERMINE_DYNAMIC_ARGS = 'WARNING : unable to determine dynamic arguments. dumping all arguments.'


def arguments_diff(arr):
    """Get indices from the 2nd dimension that may change from a 1st dimension array to another."""

    diff = []

    arr_rows_len = len(arr)
    if arr_rows_len > 0:
        arr_cols_len = len(arr[0])
        for i in range(1, arr_rows_len):
            for j in range(0, arr_cols_len):
                if arr[i][j] != arr[i - 1][j]:
                    if j not in diff:
                        diff.append(j)
        diff = sorted(diff)

    return diff


def arguments_from_job_events(arr):
    """Prepend command indice to arguments."""

    return list(map(lambda x: [str(x.id)] + x.command, arr))


def arguments_from_job_events_without_id(arr):
    """Extract command from job events."""

    return list(map(lambda x: x.command, arr))


def arguments_all(arr):
    """Get all 2nd dimension indices."""

    diff = []
    arr_rows_len = len(arr)
    if arr_rows_len > 0:
        arr_cols_len = len(arr[0])
        diff = list(range(0, arr_cols_len))
    return diff


def arguments_extract(arr, diff):
    """Extract arguments, regarding the diff indices."""

    str_list = []

    arr_rows_len = len(arr)
    if arr_rows_len > 0:
        arr_cols_len = len(arr[0])
        if not diff:
            str_list.append([ARGUMENTS_UNABLE_TO_DETERMINE_DYNAMIC_ARGS])
            diff = range(0, arr_cols_len)
        for i in range(0, arr_rows_len):
            tmp_str_list = []
            for j in range(0, arr_cols_len):
                if j in diff:
                    tmp_str_list.append(arr[i][j])
            if tmp_str_list:
                str_list.append(tmp_str_list)

    return '\n'.join(list(map(lambda x: ','.join(x), str_list)))

import fcntl
import socket
import struct


def get_ip_address(ifname) -> str:
    """Retrieve IP address in dotted quad-string format from interface name.

    Args:
        ifname: interface name

    Returns:
        ip address if device is found

    Raises:
        OSError: if network interface is not found
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    ip_address = fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15].encode('utf-8'))
    )[20:24]
    return socket.inet_ntoa(ip_address)


def get_free_port(host):
    s = socket.socket()
    s.bind((host, 0))
    port = s.getsockname()[1]
    s.close()
    return port


def get_hostname() -> str:
    return socket.gethostname()

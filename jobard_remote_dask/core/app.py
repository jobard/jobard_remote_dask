"""DaskUnitJobApp module."""
import asyncio
import contextlib
import os
import pathlib
import resource
import signal
import subprocess
import sys
from datetime import datetime, timezone
from typing import List, Dict, Optional, Tuple, Any

from distributed import Client

from jobard_remote_dask.cluster.factory import ClusterFactory
from jobard_remote_dask.core.exceptions import JobardError
from jobard_remote_dask.core.helpers.argument import (
    arguments_all,
    arguments_diff,
    arguments_extract,
    arguments_from_job_events,
    arguments_from_job_events_without_id,
)
from jobard_remote_dask.core.helpers.human import parse_human_size
from jobard_remote_dask.core.helpers.walltime import walltime_to_seconds
from jobard_remote_dask.core.logger.log import JobardLogger
from jobard_remote_dask.core.type.state import State
from jobard_remote_dask.model.event.job import JobEvent
from jobard_remote_dask.model.event.progress import JobProgressEvent


# catch all exceptions (on the asyncio loop) we cannot catch easily at runtime (like PBS submission error from Cluster)
def dask_app_handle_exception(obj, logger):
    """

    Args:
        obj:
        logger:

    Returns:

    """
    def internal_handle_exception(loop, context):  # this function is a callback. please keep the unused "loop" arg.
        msg = context.get('exception', context['message'])
        exc_context = 'Handled exception from asyncio loop {0}'.format(context)
        exc_msg = f'Caught exception: {msg}'

        log = True
        # prevent exception output if the program is being terminated, and the exception refers to failed remote tasks
        if obj.end_event is not None and \
                obj.end_event.is_set() and \
                ('exec_custom_binary' in str(msg) or 'destroyed' in str(msg)):
            log = False

        if log is True:
            logger.info(exc_context)
            logger.error(exc_msg)
        else:
            logger.debug(exc_context)
            logger.debug(exc_msg)

        logger.debug('Shutting down...')

        # anyway, request app shutdown..
        obj.shutdown_request()
    return internal_handle_exception


class DaskUnitJobApp:
    """DaskUnitJobApp class."""

    PING_INTERVAL: int = 10
    CANCEL_INTERVAL: int = 10
    PROGRESS_INTERVAL: int = 10
    DASK_CLIENT_WAIT_INTERVAL: int = 20
    DASK_CLIENT_WAIT_RESULT_TIMEOUT: int = 10
    CLUSTER_MONITOR_MIN_INTERVAL: int = 20
    CLUSTER_MONITOR_MAX_INTERVAL: int = 300

    LAUNCH_DATE_FORMAT = '%Y%m%d'

    def __init__(
            self,
            logger: JobardLogger,
            close_timeout: int,
            tcp_client,
            dask_config_file: pathlib.Path,
            loop,
            terminate_on_task_error: bool,
            enforce_resources_constraints: bool
    ):
        """
        DaskUnitJobApp constructor.

        Args:
            logger: The logger.
            close_timeout: Timeout used by closing application.
            tcp_client: The TCP Client for exchange with jobard daemon.
            dask_config_file: The path to additional Dask conf file.
            loop: The asyncio loop.
            terminate_on_task_error: If set to True: terminate the whole Jobard app if at least one task is in failed
            state.
            enforce_resources_constraints: If set to True: add CPU and memory constraints for the subprocess at the unix
             level.
        """

        # logger
        self.logger = logger

        # loop
        self.loop = loop

        # close timeout
        self.close_timeout = close_timeout

        # dask client
        self.client = None

        # dask cluster - wrapper -
        self.cluster = None

        # jobs
        self.jobs: List[JobEvent] = []

        # the jobs states
        self.job_states: Dict[int, State] = {}

        # tcp
        self.tcp_client = tcp_client

        # event
        self.end_event = asyncio.Event(loop=loop)
        self.cluster_ready_event = asyncio.Event(loop=loop)

        # progress events
        self.progress_events = []

        # config
        self.dask_config_file = dask_config_file

        # app
        self.app = None

        # app root log path
        self.app_root_log_path = None

        # jobard extra
        self.jobard_extra = {}

        # terminate on task error
        self.terminate_on_task_error = terminate_on_task_error

        # enforce resources constraints
        self.enforce_resources_constraints = enforce_resources_constraints

    def build_app_root_log_path(self) -> pathlib.Path:
        """Build the app root log path"""

        job_order_dir_level = f'{self.app.job_order_name}_O{self.app.job_order_id}' if self.app.job_order_name \
            else f'O{self.app.job_order_id}'

        return self.app.log_chroot \
            .joinpath(self.app.command_alias) \
            .joinpath(datetime.now(timezone.utc).strftime(self.LAUNCH_DATE_FORMAT)) \
            .joinpath(job_order_dir_level) \
            .joinpath(f'A{self.app.job_array_id}_T{self.app.try_id}')

    def build_job_log_files(self,
                            job_idx: int,
                            job_prefix: Optional[str]
                            ) -> Tuple[str, str]:
        """Build the job stdout and stderr files"""

        job_stdout_log = f'{job_prefix}_{job_idx}_stdout.log' if job_prefix \
            else f'{job_idx}_stdout.log'
        job_stderr_log = f'{job_prefix}_{job_idx}_stderr.log' if job_prefix \
            else f'{job_idx}_stderr.log'
        return job_stdout_log, job_stderr_log

    def create_log_path(self, name: str) -> pathlib.Path:
        """Create log directory."""

        # build the cluster log path
        cluster_log = self.app_root_log_path.joinpath(name)

        self.logger.info('creating directory ' + name + ' : ' + cluster_log.as_posix())

        # use = to ease partitions reading for logs (from Panda/Dask or Spark for instance)
        cluster_log.mkdir(parents=True, exist_ok=True)

        return cluster_log

    def submit_job(self,
                   task: JobEvent,
                   func: Any,
                   jobs_log: pathlib.Path,
                   work_log: pathlib.Path,
                   per_job_timeout: int
                   ):
        """Submit a job and create a future."""
        (job_stdout_file, job_stderr_file) = self.build_job_log_files(
            job_idx=task.id, job_prefix=task.job_prefix)

        future = self.client.submit(
            # dask task
            func,

            # task arguments
            task.command,
            job_stdout_file,
            job_stderr_file,
            jobs_log.as_posix() + pathlib.os.sep,
            work_log.as_posix() + pathlib.os.sep,
            per_job_timeout,
            parse_human_size(self.app.worker_memory),
            self.enforce_resources_constraints,
            # resource constraints at the Dask scheduler level
            resources={
                'CPU': self.app.worker_cores,
                'MEM': parse_human_size(self.app.worker_memory) - 1024 * 1024  # take a 1MiB margin
            }
        )
        return future

    async def cleanup(self):
        """Cleanup resources used by this Dask App."""

        self.logger.info('terminating the cluster and the client, please wait..')

        # unlock all waiters on events
        self.shutdown_request()

        try:
            # terminate the cluster
            if self.cluster is not None:
                await self.cluster.close_cluster()

        except Exception as e:
            self.logger.debug('Cleanup, non fatal error on cluster cleanup :')
            self.logger.debug(str(e))

        try:

            # terminate the client
            if self.client is not None:
                await self.client.close(timeout=self.close_timeout)

        except Exception as e:
            self.logger.debug('Cleanup, non fatal error on client cleanup :')
            self.logger.debug(str(e))

        try:
            # force last progress events to be flushed through TCP, before terminating the whole app
            await self.send_progress()

            # check the remaining new or submitted jobs
            # for which the log file exists (a test on stdout is sufficient)
            new_and_submitted_jobs = {k: v for k, v in self.job_states.items() if v in (State.SUBMITTED, State.NEW)}

            jobs_log_dir = self.app_root_log_path.joinpath('jobs')

            for job_id, state in new_and_submitted_jobs.items():

                # Find the job prefix of the job
                job_prefix = None
                for j in self.jobs:
                    if j.id == job_id:
                        job_prefix = j.job_prefix
                        break

                (log_file_stdout, log_file_stderr) = self.build_job_log_files(job_idx=job_id, job_prefix=job_prefix)
                # check log file existence
                if pathlib.Path(
                        os.path.join(jobs_log_dir.as_posix() + pathlib.os.sep + log_file_stdout)).exists():
                    # if log path exists, send a progress event to the daemon with failed status because remaining
                    # new or submitted jobs will not be executed
                    self.logger.debug(f'Cleanup, send failed progress event for job : {job_id}')
                    self.progress_events.append(
                        JobProgressEvent(
                            id=job_id,
                            progress=1.0,
                            state=State.FAILED,
                            job_prefix=job_prefix
                        )
                    )

            # force the new progress events to be flushed through TCP, before terminating the whole app
            await self.send_progress()

        except Exception as e:
            self.logger.debug('Cleanup, non fatal error on tcp progress events flush :')
            self.logger.debug(str(e))

        self.logger.info('Cleanup, done')

    async def ping(self):
        """Ping task : send a heartbeat regularly to tell the Jobard Daemon this remote app is still alive."""

        self.logger.info('Ping task, starting')

        try:
            # while the end_event is not set
            while not self.end_event.is_set():
                await asyncio.sleep(self.PING_INTERVAL)

                # do ping
                self.logger.info('Ping task, sending ping')
                await self.tcp_client.ping()
        finally:
            # force unlock other tasks in case of exception
            self.shutdown_request()

        self.logger.info('Ping task, ping task end')

    def shutdown_request(self):
        """Force coroutines unlock and signal we want to shut down the whole app"""

        self.end_event.set()
        self.cluster_ready_event.set()

    async def bind_cluster(self):
        """Cluster task : bind the Dask cluster."""

        self.logger.info('starting the Dask Cluster task')

        # create the log directory
        log_dir = self.create_log_path('cluster')

        # get the cluster factory
        factory = ClusterFactory()

        # create the corresponding cluster object
        self.cluster = factory.create(
            dask_config_file=self.dask_config_file,
            app=self.app,
            loop=self.loop,
            log_dir=log_dir,
            close_timeout=self.close_timeout,
            end_event=self.end_event
        )

        monitoring_interval = max(self.CLUSTER_MONITOR_MIN_INTERVAL, walltime_to_seconds(self.app.walltime) // 60)
        monitoring_interval = min(self.CLUSTER_MONITOR_MAX_INTERVAL, monitoring_interval)

        self.logger.info(
            'Cluster monitoring, setting interval to ' + str(monitoring_interval) +
            's (based on a given container walltime)'
        )

        try:

            # start the cluster
            await self.cluster.start_cluster()

            # set the cluster state as ready
            self.cluster_ready_event.set()

            # wait until the end event
            while not self.end_event.is_set():

                with contextlib.suppress(asyncio.TimeoutError):
                    await asyncio.wait_for(self.end_event.wait(), timeout=monitoring_interval)
                # and keep an eye on the cluster
                try:
                    await self.cluster.monitor_cluster()
                except JobardError as je:
                    # a JobardError is considered critical at this point
                    self.shutdown_request()
                    self.logger.error(str(je))
                    break

        finally:
            # unlock all waiters on events
            self.shutdown_request()

            try:
                # shutdown the cluster
                await self.cluster.close_cluster()
                self.cluster = None
            except Exception as e:
                self.logger.warn(str(e))

            self.logger.info('Dask Cluster task end')

    def get_first_n_elements(self, lst: List, n: int) -> List:
        """
        Get the first n elements of a list.
        Args:
            lst: The list to extract.
            n: The number of elements to extract.

        Returns:
            A list containing the elements.
        """
        result = []
        for _ in range(n):
            if lst:
                result.append(lst.pop(0))
            else:
                break
        return result

    async def dask_client(self):
        """Dask client logic."""

        # inner function, to avoid Dask serialization issues
        def exec_custom_binary(
                command: List[str],
                stdout_file: str,
                stderr_file: str,
                jobs_log_dir: str,
                work_log_dir: str,
                timeout_seconds: int,
                memory_bytes: int,
                enforce_resources_constraints: bool
        ):
            is_success = False

            # resource constraint for the subprocess at the unix level
            def pre_exec_fn():
                print('pid %d : setting session ID leader (PID == SID) ..' % os.getpid(), file=sys.stderr)
                sys.stdout.flush()
                os.setsid()  # popen start_new_session=True
                if timeout_seconds > 0:
                    # Try to set tome CPU time limit in seconds related to the process timeout.
                    print('pid %d : setting CPU time (seconds) ..' % os.getpid(), file=sys.stderr)
                    sys.stdout.flush()
                    resource.setrlimit(resource.RLIMIT_CPU, (timeout_seconds, timeout_seconds))  # tuple( soft, hard )
                print('pid %d : setting Address space ..' % os.getpid(), file=sys.stderr)
                sys.stdout.flush()
                # The maximum size of the process's virtual memory (address space) in bytes.
                resource.setrlimit(resource.RLIMIT_AS, (memory_bytes, memory_bytes))  # tuple( soft, hard )
                print('pid %d : resource constraints applied' % os.getpid(), file=sys.stderr)

            with open(os.path.join(jobs_log_dir + stdout_file), 'w+') as stdout, \
                    open(os.path.join(jobs_log_dir + stderr_file), 'w+') as stderr:

                # preserve OS ENV variables
                new_env = os.environ.copy()

                # add custom env variables
                # TODO : faire remonter les variables d'environnement
                new_env['HDF5_USE_FILE_LOCKING'] = 'FALSE'

                try:
                    # build subprocess arguments
                    subprocess_args = {

                        # command to launch
                        'args': command,

                        # log output
                        'stdout': stdout,

                        # error output
                        'stderr': stderr,

                        # current directory
                        'cwd': work_log_dir,

                        # no shell
                        'shell': False,

                        # env variables
                        'env': new_env
                    }

                    # add resources constraints if specified
                    if enforce_resources_constraints:
                        subprocess_args['preexec_fn'] = pre_exec_fn

                    with subprocess.Popen(**subprocess_args) as p:
                        try:
                            # per unit job timeout
                            ret = p.wait(timeout=(None if timeout_seconds < 0 else timeout_seconds))
                        except Exception as error:
                            stderr.write(str(error))
                            # re-raise
                            raise e
                        finally:
                            # try to terminate at once the process (and possibly all of its children)
                            try:
                                # we terminate here the whole session (PID == SID) ...
                                os.killpg(os.getpgid(p.pid), signal.SIGTERM)
                                p.terminate()
                            except OSError:
                                #  do not write to stderr here, to avoid any misunderstanding
                                #  stderr.write(str(e))
                                pass
                except Exception as error:
                    stderr.write(str(error))
                    raise

                if ret != 0:
                    if ret < 0:
                        written = stdout.write('KILLED by signal' + str(-ret))
                    else:
                        written = stdout.write('FAILED with return code' + str(ret))
                else:
                    is_success = True
                    written = stdout.write('SUCCESS!')

                if written <= 0:
                    raise RuntimeError('unable to write to stdout')
                written = stdout.write('\n')
                if written <= 0:
                    raise RuntimeError('unable to write to stdout')

            return is_success

        self.logger.info('Dask client, starting the task')

        # create the log directory
        self.logger.info('Dask client, pre-actions, creating directories ..')
        jobs_log = self.create_log_path('jobs')
        work_log = self.create_log_path('work')

        # dump all commands with ID to a single file
        all_commands = work_log.joinpath('all.cmd')
        self.logger.info('Dask client, pre-actions, dumping commands to ' + all_commands.as_posix())
        all_commands_array = arguments_from_job_events(self.jobs)
        all_commands.write_text(arguments_extract(all_commands_array, arguments_all(all_commands_array)))

        # check job timeout
        per_job_timeout: int = -1
        if 'per_job_timeout' in self.jobard_extra:
            per_job_timeout = self.jobard_extra['per_job_timeout']

        self.logger.info('Dask client, pre-actions done')

        # on cluster ready event
        await self.cluster_ready_event.wait()

        self.logger.info('Dask client, cluster ready')

        if not self.end_event.is_set():

            try:

                # create the client
                self.client = Client(address=self.cluster.cluster, asynchronous=True)

                tasks = {}
                futures = []  # avoid large numbers of futures here

                self.logger.info('Dask client, submitting jobs to scheduler ..')

                failed_jobs = []
                total_tasks = len(self.jobs)
                succeded_tasks = 0
                failed_tasks = 0

                # get the N first jobs to submit, accordingly to Dask workers number.
                tasks_remaining = len(self.jobs)
                task_to_submit = self.jobs.copy()

                tasks_tmp = self.get_first_n_elements(
                    task_to_submit,
                    tasks_remaining if tasks_remaining < self.app.n_workers else self.app.n_workers)

                # submit the first N jobs
                for task in tasks_tmp:

                    # check regularly if the end event is set
                    if self.end_event.is_set() is True:
                        self.logger.info('Dask client, end event detected. terminating ..')
                        self.shutdown_request()
                        return

                    # create the future
                    future = self.submit_job(task=task,
                                             func=exec_custom_binary,
                                             jobs_log=jobs_log,
                                             work_log=work_log,
                                             per_job_timeout=per_job_timeout
                                             )
                    tasks[future.key] = task.id
                    futures.append(future)

                    self.progress_events.append(
                        JobProgressEvent(id=task.id, progress=0.0, state=State.SUBMITTED, job_prefix=task.job_prefix)
                    )

                self.logger.info(f'Dask client, {len(futures)} jobs submitted.')

                # listen on completed future
                while len(futures) > 0:

                    self.logger.info('Dask client, waiting for results..')

                    await asyncio.wait(
                        futures,
                        timeout=self.DASK_CLIENT_WAIT_INTERVAL,
                        return_when=asyncio.FIRST_COMPLETED
                    )

                    remove = []
                    for i in range(len(futures)):

                        # check regularly if the end event is set
                        if self.end_event.is_set() is True:
                            self.logger.info('Dask client, end event detected. terminating ..')
                            self.shutdown_request()
                            return

                        future = futures[i]

                        if future.done():

                            # signal to the cluster object that the client see at least one task done
                            self.cluster.set_one_task_done()

                            # find the job
                            job_id = tasks[future.key]
                            job = None
                            for job in self.jobs:
                                if job.id == job_id:
                                    break

                            try:
                                result = await asyncio.wait_for(future, timeout=self.DASK_CLIENT_WAIT_RESULT_TIMEOUT)

                                state = (State.SUCCEEDED if result is True else State.FAILED)

                                if state is State.SUCCEEDED:
                                    succeded_tasks += 1
                                else:
                                    failed_tasks += 1
                                    failed_jobs.append(job)

                                self.logger.info(
                                    'Dask client, end of job ' + str(job.id) +
                                    ', jobard state = ' + state +
                                    ', remaining = ' + str(tasks_remaining - 1 - len(remove)) + '/' + str(total_tasks) +
                                    ', failed = ' + str(failed_tasks) + '/' + str(total_tasks) +
                                    ', success = ' + str(succeded_tasks) + '/' + str(total_tasks)
                                )

                                if self.terminate_on_task_error is True:
                                    if state is State.FAILED:
                                        err = 'TERMINATING the whole Jobard app if at least one task is in failed state'
                                        self.logger.error(err)
                                        raise JobardError(err)

                                self.progress_events.append(
                                    JobProgressEvent(
                                        id=job.id,
                                        progress=1.0,
                                        state=state,
                                        job_prefix=job.job_prefix
                                    )
                                )

                                remove.append(i)
                                await future.cancel()
                                future.release()
                                await self.client.cancel([future])

                            except asyncio.TimeoutError as e:
                                self.logger.info(
                                    'Dask client, timeout when trying to get the future result' + str(job.id)
                                )
                                self.logger.debug(str(e))

                    j = 0
                    for i in sorted(remove):
                        del futures[i - j]
                        j += 1

                    tasks_remaining -= len(remove)
                    if j > 0:
                        tasks_tmp = self.get_first_n_elements(task_to_submit, j)
                        for task in tasks_tmp:
                            # check regularly if the end event is set
                            if self.end_event.is_set() is True:
                                self.logger.info('Dask client, end event detected. terminating ..')
                                self.shutdown_request()
                                return

                            # create the future
                            future = self.submit_job(task=task,
                                                     func=exec_custom_binary,
                                                     jobs_log=jobs_log,
                                                     work_log=work_log,
                                                     per_job_timeout=per_job_timeout
                                                     )
                            tasks[future.key] = task.id
                            futures.append(future)

                            self.progress_events.append(
                                JobProgressEvent(id=task.id, progress=0.0, state=State.SUBMITTED,
                                                 job_prefix=task.job_prefix)
                            )

                        if len(tasks_tmp) > 0:
                            self.logger.info(f'Dask client, {len(tasks_tmp)} jobs submitted.')

                # dump all commands with ID to a single file
                failed_args = work_log.joinpath('failed.args')
                if self.terminate_on_task_error is False and failed_jobs:
                    self.logger.info('Dask client, post-actions, dumping failed args to ' + failed_args.as_posix())
                    all_jobs_cmd = arguments_from_job_events_without_id(self.jobs)
                    failed_jobs_cmd = arguments_from_job_events_without_id(failed_jobs)
                    failed_args.write_text(arguments_extract(failed_jobs_cmd, arguments_diff(all_jobs_cmd)))
                    self.logger.info('Dask client, post-actions done')

            finally:
                # unlock all waiters on events
                self.shutdown_request()
                self.logger.info('Dask Client task end')

    async def cancel(self):
        """Cancel task : a cancel event, shutdown the whole application."""

        self.logger.info('Cancel task, starting')

        try:
            # while the end_event is not set
            while not self.end_event.is_set():
                await asyncio.sleep(self.CANCEL_INTERVAL)

                self.logger.info('Cancel task, sending cancel check')

                to_cancel = await self.tcp_client.get_jobs_to_cancel()
                if len(to_cancel) > 0:
                    raise JobardError('Cancel task, cancel event received. shutdown the whole application now..')
        finally:
            # unlock all waiters on events
            self.shutdown_request()

        self.logger.info('Cancel task, ping cancel end')

    async def send_progress(self):
        """Send progress through TCP."""

        # check if we have progress event to send back to the daemon
        events = None
        if len(self.progress_events) > 0:
            events = self.progress_events.copy()
            self.progress_events.clear()

        if events is not None:
            # set the job states
            for event in events:
                self.logger.debug(f'Progress task, set job {event.id} to state : {event.state}')
                self.job_states[event.id] = event.state

            self.logger.info('Progress task, reporting progress')
            await self.tcp_client.report_progress(events)

    async def progress(self):
        """Progress task."""

        self.logger.info('Progress task, start progress report task')

        try:

            # while the end_event is not set
            while not self.end_event.is_set():

                # sleep a bit
                await asyncio.sleep(self.PROGRESS_INTERVAL)

                self.logger.info('Progress task, check for progress report')

                # send progress events through TCP
                await self.send_progress()

        finally:
            # unlock all waiters on events
            self.shutdown_request()

        self.logger.info('Progress task, progress report task end')

    async def run(self):

        # get app config
        self.app = await self.tcp_client.get_app()

        # extract jobard extras
        extra = list(filter(lambda x: '=' in x, self.jobard_extra))
        keys = list(map(lambda x: x.split('=')[0], extra))
        values = list(map(lambda x: x.split('=')[1], extra))
        self.jobard_extra = dict(zip(keys, values))

        # create the root log path for the app
        self.app_root_log_path = self.build_app_root_log_path()
        self.logger.info('app_root_log_path = ' + str(self.app_root_log_path))

        # get jobs to run
        self.jobs = await self.tcp_client.get_commands()

        # init the jobs states
        for job in self.jobs:
            self.job_states[job.id] = State.NEW

        # log the application name
        self.logger.info('app_name = ' + self.app.name)

        # log the number of jobs to process
        self.logger.info('n_jobs = ' + str(len(self.jobs)))

        # log the image and mount points if defined
        if self.app.image:
            self.logger.info(f'image = {self.app.image}')
            if self.app.docker_mount_points is not None:
                for key, (source, destination) in self.app.docker_mount_points.items():
                    self.logger.info(f'mount point = {key} , source = {source} , destination = {destination}')

        # create tasks
        ping_task = asyncio.create_task(self.ping())
        bind_cluster_task = asyncio.create_task(self.bind_cluster())
        dask_client_task = asyncio.create_task(self.dask_client())
        progress_task = asyncio.create_task(self.progress())
        cancel_task = asyncio.create_task(self.cancel())

        # gather
        await asyncio.gather(
            ping_task,
            bind_cluster_task,
            dask_client_task,
            progress_task,
            cancel_task
        )

"""HTC Driver class."""
import json
import pathlib

import dask
import dask_jobqueue

from jobard_remote_dask.cluster.base import ClusterBase
from jobard_remote_dask.core.exceptions import JobardClusterCriticalError, JobardError
from jobard_remote_dask.core.helpers.asynchronous import async_popen
from jobard_remote_dask.core.helpers.human import parse_human_size
from jobard_remote_dask.core.type.cluster import ClusterDaskConfig
from jobard_remote_dask.model.event.app import AppEvent


class HTCondorCluster(ClusterBase):
    """PBS Driver class."""

    HTCONDOR_STAT_CMD = 'condor_q'
    HTCONDOR_STAT_HISTORICAL_CMD = 'condor_history'
    HTCONDOR_STAT_CMD_TIMEOUT = 60

    # HtCondor Job Status
    # 0	Unexpanded	    U
    # 1	Idle	        I
    # 2	Running	        R
    # 3	Removed	        X
    # 4	Completed	    C
    # 5	Held	        H
    # 6	Submission_err	E
    HTCONDOR_ALLOWED_STATES = [1, 2]
    HTCONDOR_ALLOWED_HISTORICAL_STATES = [2, 4]  # we conserve R state here because of cache issues

    def __init__(self, dask_config_file: pathlib.Path, app: AppEvent, close_timeout: int, loop, log_dir: pathlib.Path):
        """PBS Driver constructor."""

        # set the log dir
        self.log_dir = log_dir

        # call super constructor
        super().__init__(dask_config_file=dask_config_file, app=app, close_timeout=close_timeout, loop=loop)
        self.cluster_dask_config = ClusterDaskConfig.HTCONDOR
        super().set_dask_configuration(no_error=True)

    async def start_cluster(self):

        # build cluster arguments
        arguments = {

            # log directory for both workers output and jobs output
            'log_directory': self.log_dir.as_posix() + pathlib.os.sep,

            # async usage
            'asynchronous': True,

            # avoid some bugs, by setting again the interface
            'interface': dask.config.get('jobqueue.%s.interface' % 'htcondor'),

            # disk
            'disk': str(parse_human_size(self.app.worker_disk)) + 'B',

            # number of cores per worker
            'cores': self.app.worker_cores,  # avoid any threads

            # amount of memory per worker
            'memory': str(parse_human_size(self.app.worker_memory)) + 'B',

            # additional arguments to pass to dask-worker
            'extra': [
                '--no-dashboard',
                '--no-nanny',
                '--resources',
                'CPU=' + str(self.app.worker_cores) + ',MEM=' + str(parse_human_size(self.app.worker_memory))
            ],

            # set random port for dashboard
            'scheduler_options': {'dashboard_address': None},
        }

        # set common options
        self.job_queue_opt_routine(arguments)

        # create the cluster
        self.cluster = await dask_jobqueue.HTCondorCluster(**arguments)

        # log some cluster info
        self.job_queue_log_routine()

        # scale
        await self.job_queue_scale_routine()

    async def common_condor_stat(self, cmd, worker, is_history):
        """Common condor stat routine."""

        arguments = [
            cmd,
            '-json',
        ]

        extra_log_text = ''
        if is_history:
            extra_log_text = 'HISTORY '
        else:
            arguments.append('-nobatch')

        arguments.append(str(worker.job_id))

        self._logger.info(
            'Cluster monitoring, DASK-WORKER STATUS (HTCONDOR ' + extra_log_text + 'JOB) - ' +
            str(worker.job_id) + ' - checking ..'
        )

        returncode, stdout, stderr = await async_popen(
            *arguments,
            timeout=self.HTCONDOR_STAT_CMD_TIMEOUT,
            custom_logger=self._logger
        )

        if returncode != 0:
            self._logger.debug(stderr)
            self._logger.debug(stdout)
            raise JobardError(
                'Cluster monitoring, unable to retrieve proper ' + self.HTCONDOR_STAT_CMD +
                ' output for JOB_ID ' + str(worker.job_id) + ', exit code = ' + str(returncode)
            )

        self._logger.info(
            'Cluster monitoring, DASK-WORKER STATUS (HTCONDOR {0}JOB) - {1} - {2} return code {3}'.format(
                extra_log_text,
                self.HTCONDOR_STAT_CMD,
                worker.job_id,
                returncode
            )
        )

        try:
            ret = json.loads(stdout)

            status = int(ret[0]['JobStatus'])

            # check returned job id consistency
            if str(ret[0]['JobId']) != str(worker.job_id):
                raise JobardClusterCriticalError(
                    'Cluster monitoring, wrong job id ' + str(ret[0]['JobId']) +
                    ', expected : ' + str(worker.job_id)
                )

            # check condor status
            if status not in self.HTCONDOR_ALLOWED_STATES:
                raise JobardClusterCriticalError(
                    'Cluster monitoring, incorrect container state ' + str(status) +
                    ' for container ID = ' + str(worker.job_id) + '. aborting.'
                )

            is_running = (status == 2)

        except JobardClusterCriticalError as e:
            raise e
        except JobardError as e:
            raise e
        except Exception as e:
            self._logger.debug(str(e))
            raise JobardError(
                'Cluster monitoring, unable to parse ' + self.HTCONDOR_STAT_CMD + ' cmd json output'
            )

        return is_running

    async def job_queue_check_container(self, worker):
        """
            Check the state of a running, valid, container

            Trigger a JobardError Exception, if there is a non-critical issue (one of the container is down).
            Trigger a JobardClusterCriticalError, if there is a critical issue, and the Dask app must be terminated.

            Return True, if the job is running.
            Return False, if the job is enqueued, but not running yet.
        """

        return await self.common_condor_stat(self.HTCONDOR_STAT_CMD, worker, False)

    async def job_queue_check_container_historical_state(self, worker):
        """
            If the previous method can't get the state of the container (because it is finished),
            this method provide a way, to check its final state, with some kind of "history" command.

            Trigger a JobardClusterCriticalError, if there is a critical problem, and the Dask app must be terminated.
        """
        await self.common_condor_stat(self.HTCONDOR_STAT_HISTORICAL_CMD, worker, True)

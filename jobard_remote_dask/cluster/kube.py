"""Kubernetes Driver class."""
import asyncio
import pathlib
import random
import uuid
from typing import List

import dask
import distributed
import kr8s.asyncio
from filelock import FileLock
from kr8s import NotFoundError
from kr8s.asyncio.objects import Service, Namespace, Pod

from jobard_remote_dask.cluster.base import ClusterBase
from jobard_remote_dask.core.exceptions import JobardClusterCriticalError, JobardError, JobardTimeoutError
from jobard_remote_dask.core.helpers.cpu import to_nanocores
from jobard_remote_dask.core.helpers.human import parse_human_size
from jobard_remote_dask.core.helpers.net import get_free_port, get_ip_address
from jobard_remote_dask.core.type.cluster import ClusterDaskConfig
from jobard_remote_dask.model.event.app import AppEvent
from jobard_remote_dask.model.kube.node import NodeInfos
from jobard_remote_dask.model.kube.pod import PodInfos


class KubernetesCluster(ClusterBase):
    """Kubernetes Driver class."""

    # Kubernetes POD ID max length
    CONTAINER_ID_LEN: int = 16

    # Kubernetes CPU Nano definition
    CPU_NANO: int = 1000000000

    # Sleep between two check for pods readiness
    CHECK_CONTAINERS_READINESS_AFTER_SUBMIT_SLEEP_INTERVAL_SECONDS: int = 10

    # Name of the Jobard worker node port in the Kubernetes Service spec
    JOBARD_WORKER_TCP_PORT_NAME = 'jobard-worker-np'

    # Exponential backoff algorithm to retry k8s pods creation
    # Keep in mind that the total time is determined by
    RETRY_CONTAINERS_SUBMIT_N: int = 16  # about 18 hours MAX

    # TCP Mapping Server global timeout
    TCP_MAPPING_SERVER_GLOBAL_TIMEOUT: int = 40

    # TCP Mapping Server encoding scheme
    TCP_MAPPING_SERVER_ENCODING_SCHEME: str = 'ascii'

    # KR8S API requests global timeout
    KR8S_API_REQUESTS_GLOBAL_TIMEOUT: int = 60

    def __init__(
            self,
            dask_config_file: pathlib.Path,
            app: AppEvent,
            close_timeout: int,
            loop,
            log_dir: pathlib.Path,
            end_event: asyncio.Event
    ):
        """K8S Driver constructor."""

        # K8s service name
        self.service_name = None

        # set the log dir
        self.log_dir = log_dir

        # interval simple tcp server to adjust dask-worker contact-address, regarding
        self.mapping_tcp = None

        # End event, set when the whole application must shut down
        self.end_event = end_event

        # K8S service and pods
        self.api = kr8s.api()
        self.k8s_services = {}

        # call super constructor
        super().__init__(dask_config_file=dask_config_file, app=app, close_timeout=close_timeout, loop=loop)
        self.cluster_dask_config = ClusterDaskConfig.KUBE
        super().set_dask_configuration()

    async def job_queue_check_container(self, worker):
        """
            Check the state of a running, valid, container

            Trigger a JobardError Exception, if there is a non-critical issue (one of the container is down).
            Trigger a JobardClusterCriticalError, if there is a critical issue, and the Dask app must be terminated.

            Return True, if the job is running.
            Return False, if the job is enqueued, but not running yet.
        """

        # ignore
        return True

    async def job_queue_check_container_historical_state(self, worker):
        """
            If the previous method can't get the state of the container (because it is finished),
            this method provide a way, to check its final state, with some kind of "history" command.

            Trigger a JobardClusterCriticalError, if there is a critical problem, and the Dask app must be terminated.
        """

        # ignore

    async def check_replicas(self):
        """Check replicas."""

        # Get POD status trough K8S API client
        pods = await asyncio.wait_for(
            kr8s.asyncio.get("pods", namespace=f'{self.service_name}'),
            timeout=self.KR8S_API_REQUESTS_GLOBAL_TIMEOUT
        )

        pods_ready = []
        for pod in pods:
            await asyncio.wait_for(
                pod.refresh(),
                timeout=self.KR8S_API_REQUESTS_GLOBAL_TIMEOUT
            )
            is_ready = await asyncio.wait_for(
                pod.ready(),
                timeout=self.KR8S_API_REQUESTS_GLOBAL_TIMEOUT
            )
            if is_ready:
                pods_ready.append(is_ready)

        self._logger.info(f'Cluster monitoring, up replicas : {len(pods_ready)}/{len(pods)}')

        return 0 < len(pods_ready) == len(pods)

    async def monitor_cluster(self):
        """Monitor the cluster"""
        # override the entire monitoring system in case of Kurbernetes
        # there are no adaptive scaling with this Kubernetes Driver, so we just have to check if replicas are up or not
        ret = await self.check_replicas()
        if ret is False:
            raise JobardError('Cluster monitoring, at least one replicas has gone. error. terminating app..')

        # signal that at list a container is up
        self.set_one_container_up()

        # do scheduler health check
        self.common_scheduler_health_check(self.cluster.scheduler)

    async def get_available_nodes(self):
        """Get Kubernetes available nodes."""

        # get available Kubernetes nodes
        try:
            nodes = await asyncio.wait_for(
                kr8s.asyncio.get('nodes'),
                timeout=self.KR8S_API_REQUESTS_GLOBAL_TIMEOUT
            )
        except Exception as e:
            raise JobardError('Cluster, unable to check Kubernetes nodes')

        # extract each node's hostname, and resources.
        available_k8s_nodes = []
        for node in nodes:
            # check if the node accepts pods (ie no taint set to NoSchedule)
            node_schedulable = not node.unschedulable
            if 'taints' in node.spec.keys():
                node_schedulable = all('NoSchedule' not in taint.effect for taint in node.spec.taints)

            state_ready = any('Ready' in c.type and c.status == 'True' for c in node.status.conditions)
            # if the node is ready and accept pods
            if node_schedulable and state_ready:
                available_k8s_nodes.append(NodeInfos(
                    hostname=str(next((adr.address for adr in node.status.addresses if adr.type == 'Hostname'), None)),
                    cpu_declared=to_nanocores(node.status.allocatable.cpu),
                    mem_declared=parse_human_size(node.status.allocatable.memory),
                ))

        return available_k8s_nodes

    async def get_available_resources(self, k8s_nodes):
        """Get available resources on Kubernetes cluster."""

        # get available Kubernetes pods
        pod_resources = []
        try:
            for node in k8s_nodes:
                pods = await asyncio.wait_for(
                    kr8s.asyncio.get('pods',
                                     field_selector={'spec.nodeName': node.hostname}, namespace='all'),
                    timeout=self.KR8S_API_REQUESTS_GLOBAL_TIMEOUT
                )
                for pod in pods:
                    await asyncio.wait_for(
                        pod.refresh(),
                        timeout=self.KR8S_API_REQUESTS_GLOBAL_TIMEOUT
                    )
                    is_ready = await asyncio.wait_for(
                        pod.ready(),
                        timeout=self.KR8S_API_REQUESTS_GLOBAL_TIMEOUT
                    )
                    if is_ready:
                        is_jobard = True if 'jobard' in pod.metadata.labels else False
                        cpu_declared = sum(to_nanocores(container.resources.requests.cpu)
                                           for container in pod.spec.containers if 'requests' in container.resources)
                        mem_declared = sum(parse_human_size(container.resources.requests.memory)
                                           for container in pod.spec.containers if 'requests' in container.resources)
                        pod_resources.append(PodInfos(
                            id=pod.name,
                            # check if the pod is related to jobard
                            is_jobard_unit=is_jobard,
                            cpu_declared=cpu_declared,
                            mem_declared=mem_declared,
                        ))
        except Exception as e:
            raise JobardError(f'Cluster, unable to retrieve Kubernetes pods : {e}')

        return pod_resources

    # TODO CR : Factorizate with Swarm (in a new intermediate class)
    async def check_node_resources(self) -> bool:
        """Check nodes resources availability for the given app."""

        # get cluster limits for jobard from config
        cluster_max_vcpu_for_jobard = dask.config.get('k8s.cluster_max_vcpu_for_jobard')
        cluster_max_memory_for_jobard = dask.config.get('k8s.cluster_max_memory_for_jobard')

        # determine cluster resources allocated for jobard
        cluster_max_cpu_nanos_for_jobard = cluster_max_vcpu_for_jobard * self.CPU_NANO
        cluster_max_memory_for_jobard = parse_human_size(cluster_max_memory_for_jobard)

        # determine requested resources
        cores_requested = self.app.worker_cores * self.app.n_workers
        mem_requested = parse_human_size(self.app.worker_memory) * self.app.n_workers

        self._logger.info('Cluster, REQUESTED RESOURCES. ' + str(cores_requested) + ' CORES. ' + str(
            mem_requested // (1024 ** 2)
        ) + ' MEMORY MiB.')

        # get available k8s nodes info
        available_k8s_nodes = await self.get_available_nodes()

        # get total available resources
        cluster_total_memory = sum(list([node.mem_declared for node in available_k8s_nodes]))
        cluster_total_cpu = sum(list([node.cpu_declared for node in available_k8s_nodes]))

        # log the nodes available
        self._logger.info(
            'Cluster, TOTAL CLUSTER NODES AVAILABLE : ' + str(len(available_k8s_nodes))
        )

        # log the total cluster resources
        self._logger.info(
            'Cluster, TOTAL CLUSTER RESOURCES. ' +
            str(cluster_total_cpu // self.CPU_NANO) + ' CORES. ' +
            str(cluster_total_memory // (1024 ** 2)) + ' MEMORY MiB.'
        )

        # log the total cluster allocated for jobard
        self._logger.info(
            'Cluster, TOTAL CLUSTER RESOURCES FOR JOBARD. ' +
            str(cluster_max_cpu_nanos_for_jobard // self.CPU_NANO) + ' CORES. ' +
            str(cluster_max_memory_for_jobard // (1024 ** 2)) + ' MEMORY MiB.'
        )

        # if requested resources exceed cluster resources, send a critical error.
        if mem_requested > cluster_total_memory:
            self._logger.info('Cluster, REQUESTED MEMORY EXCEED TOTAL MEMORY')
            raise JobardClusterCriticalError(
                'Cluster, critical error :  REQUESTED MEMORY EXCEED TOTAL MEMORY'
            )

        if cores_requested * self.CPU_NANO > cluster_total_cpu:
            self._logger.info('Cluster, REQUESTED CORES EXCEED TOTAL CORES')
            raise JobardClusterCriticalError(
                'Cluster, critical error :  REQUESTED CORES EXCEED TOTAL CORES'
            )

        # if requested resources exceed max cluster resources allocated for Jobard, send a critical error.
        if mem_requested > cluster_max_memory_for_jobard:
            self._logger.info('Cluster, REQUESTED MEMORY EXCEED JOBARD CLUSTER ALLOCATED MEMORY')
            raise JobardClusterCriticalError(
                'Cluster, critical error :  REQUESTED MEMORY EXCEED JOBARD CLUSTER ALLOCATED MEMORY'
            )

        if cores_requested * self.CPU_NANO > cluster_max_cpu_nanos_for_jobard:
            self._logger.info('Cluster, REQUESTED CORES EXCEED JOBARD CLUSTER ALLOCATED CORES')
            raise JobardClusterCriticalError(
                'Cluster, critical error :  REQUESTED CORES EXCEED JOBARD CLUSTER ALLOCATED CORES'
            )

        # if requested resources exceed cluster resources allocated for Jobard for one node, send a critical error.
        cluster_max_memory_for_jobard_per_node = cluster_max_memory_for_jobard // len(available_k8s_nodes)
        cluster_max_cpu_nanos_for_jobard_per_node = cluster_max_cpu_nanos_for_jobard // len(available_k8s_nodes)

        if mem_requested > cluster_max_memory_for_jobard_per_node:
            self._logger.info('Cluster, REQUESTED MEMORY EXCEED JOBARD CLUSTER ALLOCATED MEMORY PER NODE')
            raise JobardClusterCriticalError(
                'Cluster, critical error :  REQUESTED MEMORY EXCEED JOBARD CLUSTER ALLOCATED MEMORY PER NODE'
            )

        if cores_requested * self.CPU_NANO > cluster_max_cpu_nanos_for_jobard_per_node:
            self._logger.info('Cluster, REQUESTED CORES EXCEED JOBARD CLUSTER ALLOCATED CORES PER NODE')
            raise JobardClusterCriticalError(
                'Cluster, critical error :  REQUESTED CORES EXCEED JOBARD CLUSTER ALLOCATED CORES PER NODE'
            )

        # if max cluster resources allocated for Jobard exceed cluster resources, send a critical error.
        if cluster_max_memory_for_jobard > cluster_total_memory:
            self._logger.info('Cluster, JOBARD CLUSTER ALLOCATED MEMORY EXCEED TOTAL CLUSTER MEMORY')
            raise JobardClusterCriticalError(
                'Cluster, critical error :  JOBARD CLUSTER ALLOCATED MEMORY EXCEED TOTAL CLUSTER MEMORY'
            )

        if cluster_max_cpu_nanos_for_jobard > cluster_total_cpu:
            self._logger.info('Cluster, JOBARD CLUSTER ALLOCATED CORES EXCEED TOTAL CLUSTER CORES')
            raise JobardClusterCriticalError(
                'Cluster, critical error :  JOBARD CLUSTER ALLOCATED CORES EXCEED TOTAL CLUSTER CORES'
            )

        # get available resources
        all_containers = await self.get_available_resources(available_k8s_nodes)

        # get the cluster currently used resources
        cluster_current_memory_used = sum(list([container.mem_declared for container in all_containers]))
        cluster_current_cpu_nanos_used = sum(list([container.cpu_declared for container in all_containers]))

        # get the cluster currently used resources for "jobarded" images
        cluster_current_cpu_nanos_used_for_jobard = sum(list([
            container.cpu_declared for container in all_containers if container.is_jobard_unit is True]))
        cluster_current_memory_used_for_jobard = sum(list([
            container.mem_declared for container in all_containers if container.is_jobard_unit is True]))

        # log the cluster currently used resources
        self._logger.info('Cluster, USED CLUSTER RESOURCES. ' +
                          str(cluster_current_cpu_nanos_used // self.CPU_NANO) + ' CORES. ' +
                          str(cluster_current_memory_used // (1024 ** 2)) + ' MEMORY MiB.')

        # log the "jobarded" currently used resources
        self._logger.info('Cluster, USED CLUSTER RESOURCES FOR JOBARD. ' +
                          str(cluster_current_cpu_nanos_used_for_jobard // self.CPU_NANO) + ' CORES. ' +
                          str(cluster_current_memory_used_for_jobard // (1024 ** 2)) + ' MEMORY MiB.')

        # check if cluster resources are enough
        if cluster_current_memory_used + mem_requested > cluster_total_memory:
            self._logger.info('Cluster, GLOBAL CLUSTER HAS NOT ENOUGH AVAILABLE MEMORY')
            return False

        if cluster_current_cpu_nanos_used + cores_requested * self.CPU_NANO > cluster_total_cpu:
            self._logger.info('Cluster, GLOBAL CLUSTER HAS NOT ENOUGH AVAILABLE CORES')
            return False

        # check if dedicated resources for jobard are enough
        if cluster_current_memory_used_for_jobard + mem_requested > cluster_max_memory_for_jobard:
            self._logger.info('Cluster, JOBARD CLUSTER HAS NOT ENOUGH AVAILABLE MEMORY')
            return False

        cores = cluster_current_cpu_nanos_used_for_jobard + cores_requested * self.CPU_NANO
        if cores > cluster_max_cpu_nanos_for_jobard:
            self._logger.info('Cluster, JOBARD CLUSTER HAS NOT ENOUGH AVAILABLE CORES')
            return False

        return True

    def create_worker_cmd_args(self, scheduler_address: str, mapping_tcp_host: str, mapping_tcp_port: int) -> List:
        """
        Create Dask worker command arguments.
        Args:
            scheduler_address: The address (IP:PORT) of the Dask scheduler.
            mapping_tcp_host: The mapping TCP server host to pass to the worker.
            mapping_tcp_port: The mapping TCP server port to pass to the worker.
        Returns:
            The list containing the arguments.
        """
        worker_cmd_args = [

            # set the binary
            '/venv/jobard_worker_dask/bin/dask-worker',

            # set the scheduler address
            scheduler_address,

            # set the preload script (redirect dask-worker's contact-address to k8s host directly)
            '--preload',
            '/venv/jobard_worker_dask/lib/python3.9/site-packages/jobard_worker_dask/swarm.py',

            # local directory
            '--local-directory',
            '/tmp/',

            # avoid any threads
            '--nthreads',
            '1',

            # automatically leave room for system OS overhead
            '--memory-limit',
            'auto',

            # listen inside the container on port 9000
            '--listen-address',
            'tcp://0.0.0.0:9000',

            # set the mapping tcp service contact point, used by the preload script
            '--mapping-tcp-host',
            str(mapping_tcp_host),
            '--mapping-tcp-port',
            str(mapping_tcp_port),

            # disable nanny
            '--no-nanny',

            # disable dashboard
            '--no-dashboard',

            # set death timeout (avoid workers to stick around needlessly if their scheduler goes away)
            '--death-timeout',
            str(self.app.death_timeout),

            # set resources
            '--resources',
            'CPU=' + str(self.app.worker_cores) + ',MEM=' + str(parse_human_size(self.app.worker_memory)),
        ]

        self._logger.info(' '.join(worker_cmd_args))

        return worker_cmd_args

    async def create_cluster(self, worker_cmd_args: List, n_workers: int):
        """
        Create the K8s Dask resources. It is composed of :
        - A namespace for all K8s Dask resources for Jobard.
        - For each dask worker :
            - A NodePort Service for networking between POD and host.
            - A POD containing the dask worker command.
        Args:
            worker_cmd_args: The arguments to pass to the dask worker command.
            n_workers: The number of dask workers to launch.
        """
        # Create the namespace
        try:
            await asyncio.wait_for(
                Namespace.get(name='jobard', namespace=f'{self.service_name}'),
                timeout=self.KR8S_API_REQUESTS_GLOBAL_TIMEOUT
            )
        except NotFoundError:
            # Sends a non-critical exception if create namespace failure, others raised exceptions by Namespace.get()
            # are considered critical
            ns = await asyncio.wait_for(Namespace({
                    'apiVersion': 'v1',
                    'kind': 'Namespace',
                    'metadata': {
                        'name': f'{self.service_name}'
                    }
                }),
                timeout=self.KR8S_API_REQUESTS_GLOBAL_TIMEOUT
            )
            try:
                await asyncio.wait_for(
                    ns.create(),
                    timeout=self.KR8S_API_REQUESTS_GLOBAL_TIMEOUT
                )
            except Exception as e:
                raise JobardError(f'Cluster, error when creating namespace : {self.service_name} : {e}')

        # Create the Dask workers
        for index in range(n_workers):
            # Create the service
            service = await asyncio.wait_for(Service(
                {
                    'apiVersion': 'v1',
                    'kind': 'Service',
                    'metadata': {
                        'name': f'jobard-service{index}',
                        'namespace': f'{self.service_name}'
                    },
                    'spec': {
                        'type': 'NodePort',
                        'selector': {
                            'app': f'jobard-app{index}'
                        },
                        'ports': [
                            {
                                'port': 9000,
                                'targetPort': 9000,
                                'name': self.JOBARD_WORKER_TCP_PORT_NAME
                            }
                        ]
                    }
                }),
                timeout=self.KR8S_API_REQUESTS_GLOBAL_TIMEOUT
            )

            try:
                await asyncio.wait_for(
                    service.create(),
                    timeout=self.KR8S_API_REQUESTS_GLOBAL_TIMEOUT
                )
            except Exception as e:
                raise JobardError(f'Cluster, error when creating service : jobard-service{index} : {e}')

            # Create a pod
            pod = await asyncio.wait_for(Pod(
                {
                    'apiVersion': 'v1',
                    'kind': 'Pod',
                    'metadata': {
                        'name': f'jobard-worker{index}',
                        'namespace': f'{self.service_name}',
                        'labels': {
                            'app': f'jobard-app{index}',
                            'jobard': 'true'
                        }
                    },
                    'spec': {
                        'containers': [
                            {
                                'name': f'jobard-worker{index}',
                                'image': self.app.image,
                                'imagePullPolicy': 'IfNotPresent',
                                # Worker command args
                                'command': worker_cmd_args,
                                # Mounting
                                'volumeMounts': [
                                    {
                                        'mountPath': str(source),
                                        'name': key
                                    } for key, (source, destination) in self.app.docker_mount_points.items()
                                ],
                                # Resources limits
                                'resources': {
                                    'requests': {
                                        'memory': f'{parse_human_size(self.app.worker_memory)}',
                                        'cpu': f'{self.app.worker_cores}'
                                    },
                                    'limits': {
                                        'memory': f'{parse_human_size(self.app.worker_memory)}',
                                        'cpu': f'{self.app.worker_cores}'
                                    }
                                }
                            }
                        ],
                        'volumes': [
                            {
                                'name': key,
                                'hostPath':
                                    {
                                        'path': str(destination)
                                    }
                            } for key, (source, destination) in self.app.docker_mount_points.items()
                        ]
                    }
                }),
                timeout=self.KR8S_API_REQUESTS_GLOBAL_TIMEOUT
            )
            try:
                await asyncio.wait_for(
                    pod.create(),
                    timeout=self.KR8S_API_REQUESTS_GLOBAL_TIMEOUT
                )
            except Exception as e:
                raise JobardError(f'Cluster, error when creating pod : jobard-worker{index} : {e}')

            # Register the service
            self.k8s_services[pod.name] = service

    async def workers_submit(self, scheduler_address, mapping_tcp_host, mapping_tcp_port) -> bool:
        """Submit and scale the APP."""

        # Create K8s Dask resources (workers)
        worker_cmd_args = self.create_worker_cmd_args(scheduler_address=scheduler_address,
                                                      mapping_tcp_host=mapping_tcp_host,
                                                      mapping_tcp_port=mapping_tcp_port)

        self.service_name = 'jobard-' + uuid.uuid4().hex
        self._logger.info(f'Cluster, setting the service name : {self.service_name}')
        await self.create_cluster(worker_cmd_args=worker_cmd_args, n_workers=self.app.n_workers)

        replicas_ready = False

        # wait until the PODs are up and running AND the whole application must not terminate ASAP
        while not self.end_event.is_set():

            services_ready = False

            if replicas_ready is False:
                ret = await self.check_replicas()
                if ret is True:
                    self._logger.info('Cluster, REPLICAS READY !')
                    replicas_ready = True

            # check the pods status
            pods = await asyncio.wait_for(
                kr8s.asyncio.get("pods", namespace=f'{self.service_name}'),
                timeout=self.KR8S_API_REQUESTS_GLOBAL_TIMEOUT
            )
            states = {}
            errors = {}
            for pod in pods:
                await asyncio.wait_for(
                    pod.refresh(),
                    timeout=self.KR8S_API_REQUESTS_GLOBAL_TIMEOUT
                )
                states[pod.name] = pod.status
                # retrieve the last error
                false_conditions = list(filter(lambda cond: 'False' in cond.status, pod.status.conditions))
                if len(false_conditions) > 0:
                    condition = sorted(
                        false_conditions, key=lambda x: x['lastTransitionTime'], reverse=True)[0]
                    errors[pod.name] = f'{condition.reason} {condition.message}'
                else:
                    errors[pod.name] = ''

            i = 0
            for pod_id, state in states.items():
                error = errors[pod_id]

                # if the state seems weird, shutdown immediately the whole Dask application
                if 'Unknown' in state.phase:
                    raise JobardClusterCriticalError(
                        f'Cluster, critical error, state of one of the pods is Unknown, while initializing : {error}')
                # If the pod is pending, log the reason for false status (ie insufficient resources) and keep waiting
                if 'Pending' in state.phase:
                    self._logger.info(f'Cluster, one pod is in Pending state, reason : {error}')
                if 'Failed' in state.phase:
                    raise JobardClusterCriticalError(
                        f'Cluster, critical error, state of one of the pods is Failed, while initializing : {error}')
                if 'Succedeed' in state.phase:
                    raise JobardClusterCriticalError(
                        f'Cluster, critical error, state of one of the pods is Succedeed, while initializing : {error}'
                    )
                # if the state is RUNNING, increment the number of replicas in RUNNING state
                if 'Running' in state.phase:
                    i = i + 1

                # for other state, we will wait, until they reach the RUNNING state

            # check if all pods are running or not
            if len(states) == i:
                self._logger.info('Cluster, ALL REPLICAS ARE UP AND RUNNING')
                services_ready = True

            # return if the two conditions (replicas and services readiness) are met
            if services_ready is True and replicas_ready is True:
                self._logger.info('Cluster, SERVICES READY!')
                break

            # do wait before the next check
            await asyncio.sleep(self.CHECK_CONTAINERS_READINESS_AFTER_SUBMIT_SLEEP_INTERVAL_SECONDS)

        return True

    async def mapping_tcp_handle(self, reader, writer):
        """Handle a new connection to the TCP server"""

        try:

            self._logger.info('Service name ' + self.service_name)

            # if the service name is defined
            if self.service_name is not None:

                # read the POD ID
                pod_id_raw = await asyncio.wait_for(
                    reader.read(self.CONTAINER_ID_LEN),
                    timeout=self.TCP_MAPPING_SERVER_GLOBAL_TIMEOUT
                )

                pod_id = pod_id_raw.decode(self.TCP_MAPPING_SERVER_ENCODING_SCHEME).strip()

                self._logger.info('Cluster, asking for pod id ' + pod_id + ' mapping information')

                # Get cluster infos trough K8S API client
                pod = await asyncio.wait_for(
                    Pod.get(namespace=f'{self.service_name}', name=pod_id),
                    timeout=self.TCP_MAPPING_SERVER_GLOBAL_TIMEOUT
                )
                await asyncio.wait_for(
                    pod.refresh(),
                    timeout=self.TCP_MAPPING_SERVER_GLOBAL_TIMEOUT
                )
                # Get the host IP of the POD
                pod_ip = str(pod.status.hostIP)
                service = self.k8s_services[pod_id]
                host_port = str(next(
                    (item.nodePort for item in service.spec.ports if item.name == self.JOBARD_WORKER_TCP_PORT_NAME),
                    None
                ))

                if pod_ip and host_port:
                    res = pod_ip + '|0.0.0.0:' + host_port + '->9000/tcp, :::' + host_port + '->9000/tcp'
                    self._logger.info('Cluster, responding back ' + res)
                    writer.write(res.encode(self.TCP_MAPPING_SERVER_ENCODING_SCHEME))
                else:
                    self._logger.warn(
                        'Cluster, unable to retrieve pod information (pod ip or host port)'
                    )
        finally:

            # tcp writer drain
            try:
                await writer.drain()
            except Exception as e:
                self._logger.warn(e)

            # graceful cleanup
            if writer is not None:
                if not writer.is_closing():
                    try:
                        writer.close()
                        await asyncio.wait_for(writer.wait_closed(), timeout=self.TCP_MAPPING_SERVER_GLOBAL_TIMEOUT)
                    except Exception as e:
                        self._logger.warn(e)

    # TODO CR : Factorizate with Swarm (in a new intermediate class)
    def check_constraints(self):
        """Validate app compatibility with Kubernetes."""

        # prevent app without any Docker image set
        if self.app.image is None:
            raise JobardError('Cluster, Docker image not set for this app')

    # TODO CR : Factorizate with Swarm (in a new intermediate class)
    async def start_cluster(self):
        """Start the cluster."""

        # first, check constraints before trying to run any pod
        self.check_constraints()

        # find free port for TCP mapping server
        mapping_tcp_host = get_ip_address(dask.config.get('k8s.public_interface'))
        mapping_tcp_port = get_free_port(mapping_tcp_host)

        # start the internal mapping tcp server
        self.mapping_tcp = await asyncio.wait_for(
            asyncio.start_server(self.mapping_tcp_handle, host=mapping_tcp_host, port=mapping_tcp_port),
            timeout=self.TCP_MAPPING_SERVER_GLOBAL_TIMEOUT
        )

        self._logger.info(
            'Cluster, internal tcp server running on tcp://' + mapping_tcp_host + ':' + str(mapping_tcp_port)
        )

        # build cluster arguments
        arguments = {

            # log directory for both workers output and jobs output
            'log_directory': self.log_dir.as_posix() + pathlib.os.sep,

            # async usage
            'asynchronous': True,

            # always use processes
            'processes': True,

            # workers will be added later
            'n_workers': 0,

            # insecure protocol supported only
            'protocol': 'tcp://',

            # set network interface
            'interface': dask.config.get('k8s.public_interface'),

            # set random ports for dashboard
            'dashboard_address': None,
        }

        # create the cluster
        self.cluster = await distributed.LocalCluster(**arguments)

        # log some cluster info
        self._logger.info(f'LOCAL CLUSTER ADDRESS : {self.cluster.scheduler.address}')
        self._logger.info(f'LOCAL CLUSTER DASHBOARD : {self.cluster.dashboard_link}')

        # get the scheduler address
        scheduler_address = self.cluster.scheduler.address

        # wait for available resources in another process
        x = 0
        k8s_lock = FileLock(dask.config.get('k8s.lock'))
        while not self.end_event.is_set():
            try:
                ret_check_resources = False
                ret_worker_submit = False

                # global lock in order to avoid resources contention
                with k8s_lock:
                    # first try to check for available resources
                    try:
                        ret_check_resources = await self.check_node_resources()
                    except JobardClusterCriticalError as jcce:
                        self._logger.error(
                            'Cluster, critical exception occured while trying to acquire cluster resources'
                        )
                        raise jcce
                    except JobardTimeoutError as jte:
                        self._logger.warn(
                            'Cluster, non-fatal timeout exception occured while trying to acquire cluster resources'
                        )
                        self._logger.warn(jte)
                    except JobardError as je:
                        self._logger.warn(
                            'Cluster, non-fatal non-timeout exception occured while trying to acquire cluster resources'
                        )
                        self._logger.warn(je)
                    except Exception as e:
                        self._logger.error(
                            'Cluster, fatal exception while trying to acquire cluster resources'
                        )
                        raise e

                    if ret_check_resources is True:
                        cleanup_needed = True
                        try:
                            ret_worker_submit = await self.workers_submit(scheduler_address, mapping_tcp_host,
                                                                          mapping_tcp_port)
                            cleanup_needed = False
                            break
                        except JobardClusterCriticalError as jcce:
                            self._logger.error(
                                'Cluster, critical exception occured while submitting Kubernetes service'
                            )
                            raise jcce
                        except JobardTimeoutError as jte:
                            self._logger.warn(
                                'Cluster, non-fatal timeout exception occured while submitting Kubernetes service'
                            )
                            self._logger.warn(jte)
                        except JobardError as je:
                            self._logger.warn(
                                'Cluster, non-fatal non-timeout exception occured while submitting Kubernetes service'
                            )
                            self._logger.warn(je)
                        except Exception as e:
                            self._logger.error(
                                'Cluster, fatal exception while submitting Kubernetes service'
                            )
                            raise e
                        finally:
                            if cleanup_needed is True:
                                await self.cleanup_containers()
            except Exception as e:
                self._logger.error(
                    'Cluster, fatal exception with Kubernetes Lock'
                )
                raise e

            if ret_check_resources is False or ret_worker_submit is False:
                if x == self.RETRY_CONTAINERS_SUBMIT_N:
                    raise JobardError(
                        'Cluster, too many tries before having resources from Kubernetes'
                    )
                else:
                    # exponential backoff
                    sleep = 2 ** x + random.uniform(0, 1)
                    self._logger.info(
                        'Cluster, WAITING FOR :' + str(sleep) + 's'
                    )
                    await asyncio.sleep(sleep)
                    x += 1

    async def backup_logs(self):
        """Backup logs to a different directory."""
        try:
            self._logger.info('Cluster, writing logs to job order\'s "cluster" sub-directory')
            # Retrieve the logs of each POD
            pods = await asyncio.wait_for(
                kr8s.asyncio.get("pods", namespace=f'{self.service_name}'),
                timeout=self.KR8S_API_REQUESTS_GLOBAL_TIMEOUT
            )

            for pod in pods:
                try:
                    self._logger.info(f'Cluster, writing logs for pod: {pod.name} / {pod.status.hostIP} ..')
                    await asyncio.wait_for(
                        pod.refresh(),
                        timeout=self.KR8S_API_REQUESTS_GLOBAL_TIMEOUT
                    )
                    logs = await asyncio.wait_for(
                        pod.logs(),
                        timeout=self.KR8S_API_REQUESTS_GLOBAL_TIMEOUT
                    )
                    # write if not already exists
                    log_file = self.log_dir.joinpath(f'{pod.name}_stdout.log')
                    if not log_file.exists():
                        # dump logs to job order "cluster" sub-directory
                        log_file.write_text(logs)
                except Exception as e:
                    self._logger.warn(
                        f'Cluster, unable to retrieve logs for the pod: {pod.name} on node : {pod.status.hostIP} '
                        + ', exception: ' + str(e))

        except Exception as error:
            self._logger.warn('Cluster, error while doing backup of pods logs')
            self._logger.warn(str(error))

    async def cleanup_containers(self):
        """cleanup/terminate the Kubernetes service."""

        self._logger.info('Cluster, dropping Kubernetes service')
        if self.service_name is not None:
            try:
                # Delete the namespace => delete all resources belonging to the namespace (service, pods...)
                ns = await asyncio.wait_for(
                    Namespace.get(name=f'{self.service_name}'),
                    timeout=self.KR8S_API_REQUESTS_GLOBAL_TIMEOUT
                )
                await asyncio.wait_for(
                    ns.delete(),
                    timeout=self.KR8S_API_REQUESTS_GLOBAL_TIMEOUT
                )
            except Exception as e:
                self._logger.warn(e)
            finally:
                self.k8s_services = {}
                self.service_name = None

    async def cleanup_mapping_tcp(self):
        """cleanup tcp mapping server."""

        if self.mapping_tcp is not None:
            try:
                self.mapping_tcp.close()
                await asyncio.wait_for(self.mapping_tcp.wait_closed(), timeout=self.TCP_MAPPING_SERVER_GLOBAL_TIMEOUT)
            except Exception as e:
                self._logger.warn(e)
            finally:
                self.mapping_tcp = None

    # TODO CR : Factorizate with Swarm (in a new intermediate class)
    async def close_cluster(self):
        """Stop the cluster."""

        await self.backup_logs()

        await self.cleanup_containers()

        await self.cleanup_mapping_tcp()

        await super().close_cluster()

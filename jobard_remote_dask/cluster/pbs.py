"""PBS Driver class."""
import pathlib
import re

import dask
import dask_jobqueue

from jobard_remote_dask.cluster.base import ClusterBase
from jobard_remote_dask.core.exceptions import JobardClusterCriticalError, JobardError
from jobard_remote_dask.core.helpers.asynchronous import async_popen
from jobard_remote_dask.core.helpers.human import parse_human_size
from jobard_remote_dask.core.type.cluster import ClusterDaskConfig
from jobard_remote_dask.model.event.app import AppEvent


class PBSCluster(ClusterBase):
    """PBS Driver class."""

    PBS_ALLOWED_STATES = ['T', 'Q', 'E', 'R', 'W']
    PBS_ALLOWED_STATES_HISTORY = ['R', 'F']  # we conserve R state here because of cache issues
    QSTAT_TIMEOUT = 120
    QSTAT_COMMAND = 'qstat'
    REGEXP_QSTAT_STATE = 'state[ ]*=[ ]*([A-Z])'
    REGEXP_QSTAT_SESSION_ID = 'session[_ -]id[ ]*=[ ]*([0-9]+)'

    def __init__(self, dask_config_file: pathlib.Path, app: AppEvent, close_timeout: int, loop, log_dir: pathlib.Path):
        """PBS Driver constructor."""

        self.log_dir = log_dir

        super().__init__(dask_config_file=dask_config_file, app=app, close_timeout=close_timeout, loop=loop)
        self.cluster_dask_config = ClusterDaskConfig.PBS
        super().set_dask_configuration(no_error=True)

    async def start_cluster(self):
        """Start the cluster."""

        # build cluster arguments
        arguments = {

            # log directory for both workers output and jobs output
            'log_directory': self.log_dir.as_posix() + pathlib.os.sep,

            # async usage
            'asynchronous': True,

            # number of cores per worker
            'cores': self.app.worker_cores,  # avoid any threads

            # number of processes per worker
            'processes': self.app.worker_cores,  # avoid any threads

            # amount of memory per worker
            'memory': self.app.worker_memory,

            # be ready for MPI (distributed nodes) queues
            'resource_spec': 'select=1:ncpus=' + str(self.app.worker_cores) + ':mem=' + self.app.worker_memory,

            # set the queue
            'queue': self.app.queue,

            # set the project name
            'project': self.app.name,

            # set the walltime per dask-worker
            'walltime': self.app.walltime,  # WARN : it is NOT per job, but for all jobs inside a given worker

            # set the dask-worker death timeout
            # avoid workers to stick around needlessly if their scheduler goes away
            'death_timeout': self.app.death_timeout,

            # avoid some bugs, by setting again the interface
            'interface': dask.config.get('jobqueue.%s.interface' % 'pbs'),

            # additional arguments to pass to dask-worker
            'extra': [
                '--no-dashboard',
                '--no-nanny',
                # set resources to avoid a given dask worker to accept more load than expected
                '--resources',
                'CPU=' + str(self.app.worker_cores) + ',MEM=' + str(parse_human_size(self.app.worker_memory)),

            ],

            # set random port for dashboard
            'scheduler_options': {'dashboard_address': None},
        }

        # set common options
        self.job_queue_opt_routine(arguments)

        # create the cluster
        self.cluster = await dask_jobqueue.PBSCluster(**arguments)

        # log some cluster info
        self.job_queue_log_routine()

        # scale
        await self.job_queue_scale_routine()

    def pbs_container_check_state(self, job_id: str, console_output, allowed_states):
        """Parse QSTAT output to extract the container state"""

        # check further the PBS container state
        output = re.search(self.REGEXP_QSTAT_STATE, console_output, flags=re.IGNORECASE)
        if output is not None:
            state_char = output.group(1)
            if state_char not in allowed_states:
                raise JobardClusterCriticalError(
                    'Cluster monitoring, incorrect container state ' + state_char +
                    ' for container ID = ' + job_id + '. aborting.'
                )

    async def job_queue_check_container(self, worker):
        """
            Check the state of a running, valid, container

            Trigger a JobardError Exception, if there is a non-critical issue (one of the container is down).
            Trigger a JobardClusterCriticalError, if there is a critical issue, and the Dask app must be terminated.

            Return True, if the job is running.
            Return False, if the job is enqueued, but not running yet.
        """
        self._logger.info(
            'Cluster monitoring, DASK-WORKER STATUS (PBS JOB) - ' + str(worker.job_id) + ' - checking ..'
        )

        returncode, stdout, stderr = await async_popen(

            self.QSTAT_COMMAND,

            # we need the full output in order to detect if the job is
            # in queue (session_id not set) or running (session_id set / job_state R)
            '-f',
            str(worker.job_id),

            timeout=self.QSTAT_TIMEOUT,
            custom_logger=self._logger

        )

        if returncode != 0:
            self._logger.debug(stderr)
            self._logger.debug(stdout)
            raise JobardError(
                'Cluster monitoring, unable to retrieve proper ' + self.QSTAT_COMMAND +
                ' output for PBS_ID ' + str(worker.job_id) + ', exit code = ' + str(returncode)
            )

        self._logger.info(
            'Cluster monitoring, DASK-WORKER STATUS (PBS JOB) - {0} - {1} return code {2}'.format(
                worker.job_id,
                self.QSTAT_COMMAND,
                returncode
            )
        )

        # the container state seems fine, but check further, if it is RUNNING, or just ENQUEUED
        is_running = False
        output = re.search(self.REGEXP_QSTAT_SESSION_ID, stdout, flags=re.IGNORECASE)
        if output is not None:
            session_id = int(output.group(1))
            if session_id > 0:
                is_running = True

        # check further the PBS container state
        self.pbs_container_check_state(str(worker.job_id), stdout, self.PBS_ALLOWED_STATES)

        return is_running

    async def job_queue_check_container_historical_state(self, worker):
        """
            If the previous method can't get the state of the container (because it is finished),
            this method provide a way, to check its final state, with some kind of "history" command.

            Trigger a JobardClusterCriticalError, if there is a critical problem, and the Dask app must be terminated.
        """

        # TODO : optimize PBS qstat call
        returncode, stdout, stderr = await async_popen(

            self.QSTAT_COMMAND,

            # we want to check the history only
            '-x',

            # we need the full output in order to detect if the job is
            # in queue (session_id not set) or running (session_id set / job_state R)
            '-f',

            str(worker.job_id),

            timeout=self.QSTAT_TIMEOUT,
            custom_logger=self._logger

        )

        if returncode != 0:
            self._logger.debug(stderr)
            self._logger.debug(stdout)
            raise JobardError(
                'Cluster monitoring, unable to retrieve proper ' + self.QSTAT_COMMAND +
                ' historical output for PBS_ID ' + str(worker.job_id) + ', exit code = ' + str(returncode)
            )

        self._logger.info(
            'Cluster monitoring, DASK-WORKER HISTORICAL STATUS (PBS JOB) - ' + str(worker.job_id) +
            ' - qstat return code ' + str(returncode)
        )

        # check further the PBS container state
        self.pbs_container_check_state(str(worker.job_id), stdout, self.PBS_ALLOWED_STATES_HISTORY)

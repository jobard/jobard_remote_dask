"""LocalCluster Driver class."""
import pathlib

import dask
import distributed

from jobard_remote_dask.cluster.base import ClusterBase
from jobard_remote_dask.core.helpers.human import parse_human_size
from jobard_remote_dask.core.type.cluster import ClusterDaskConfig
from jobard_remote_dask.model.event.app import AppEvent


class LocalCluster(ClusterBase):
    """LocalCluster Driver class."""

    def __init__(self, dask_config_file: pathlib.Path, app: AppEvent, close_timeout: int, loop, log_dir: pathlib.Path):
        """LocalCluster Driver constructor."""

        self.log_dir = log_dir

        super().__init__(dask_config_file=dask_config_file, app=app, close_timeout=close_timeout, loop=loop)
        self.cluster_dask_config = ClusterDaskConfig.LOCAL
        super().set_dask_configuration(no_error=True)

    async def start_cluster(self):
        """Start the cluster."""

        # set worker resources (worker_kargs seems to no work properly with local mode)
        dask.config.set({'distributed.worker.resources.CPU': 1})
        dask.config.set({'distributed.worker.resources.MEM': parse_human_size(self.app.worker_memory)})

        # create the cluster
        self.cluster = await distributed.LocalCluster(

            # n workers
            n_workers=0,

            # local directory
            local_directory='/tmp/jobard/',

            # async usage
            asynchronous=True,

            # always use processes
            processes=True,

            # avoid any threads
            threads_per_worker=1,

            # no Nanny
            worker_class='distributed.Worker',

            # set random ports for dashboard
            dashboard_address=None,
        )

        # log some cluster info
        self._logger.info(self.cluster.scheduler.address)
        self._logger.info(self.cluster.dashboard_link)

        # scale
        if self.app.n_workers is not None:
            await self.cluster.scale(n=self.app.n_workers)
        else:
            await self.cluster.adapt(minimum=self.app.n_min_workers, maximum=self.app.n_max_workers)

        # signal that at list a container is up
        self.set_one_container_up()

    async def job_queue_check_container(self, worker):
        """
            Check the state of a running, valid, container

            Trigger a JobardError Exception, if there is a non-critical issue (one of the container is down).
            Trigger a JobardClusterCriticalError, if there is a critical issue, and the Dask app must be terminated.

            Return True, if the job is running.
            Return False, if the job is enqueued, but not running yet.
        """
        return True

    async def job_queue_check_container_historical_state(self, worker):
        """
            If the previous method can't get the state of the container (because it is finished),
            this method provide a way, to check its final state, with some kind of "history" command.

            Trigger a JobardClusterCriticalError, if there is a critical problem, and the Dask app must be terminated.
        """

    async def monitor_cluster(self):
        """Monitor the cluster and trigger Jobard exception in case of critical failure detection."""

        # do scheduler health check
        self.common_scheduler_health_check(self.cluster.scheduler)

"""Cluster Factory class."""
import asyncio
import pathlib

from jobard_remote_dask.cluster.base import ClusterBase
from jobard_remote_dask.cluster.htc import HTCondorCluster
from jobard_remote_dask.cluster.kube import KubernetesCluster
from jobard_remote_dask.cluster.local import LocalCluster
from jobard_remote_dask.cluster.pbs import PBSCluster
from jobard_remote_dask.cluster.swarm import SwarmCluster
from jobard_remote_dask.core.exceptions import JobardError
from jobard_remote_dask.core.type.cluster import Cluster
from jobard_remote_dask.model.event.app import AppEvent


class ClusterFactory:
    """Cluster Factory class."""

    @staticmethod
    def create(
        close_timeout: int,
        dask_config_file: pathlib.Path,
        app: AppEvent,
        loop,
        log_dir: pathlib.Path,
        end_event: asyncio.Event,
    ) -> ClusterBase:
        """Create cluster instance.

        Args:
            close_timeout: timeout before terminating abruptly the cluster
            dask_config_file: path to additional Dask conf files
            app: the distributed app spec
            loop: the asyncio loop
            log_dir: the log directory
            end_event: the end event (if set, the whole Dask application must shut down)

        Returns:
            Cluster object instance
        """

        if app.cluster_type == Cluster.PBS:
            return PBSCluster(
                dask_config_file=dask_config_file,
                app=app,
                close_timeout=close_timeout,
                loop=loop,
                log_dir=log_dir
            )

        if app.cluster_type == Cluster.HTCONDOR:
            return HTCondorCluster(
                dask_config_file=dask_config_file,
                app=app,
                close_timeout=close_timeout,
                loop=loop,
                log_dir=log_dir
            )

        if app.cluster_type == Cluster.SWARM:
            return SwarmCluster(
                dask_config_file=dask_config_file,
                app=app,
                close_timeout=close_timeout,
                loop=loop,
                log_dir=log_dir,
                end_event=end_event
            )

        if app.cluster_type == Cluster.LOCAL:
            return LocalCluster(
                dask_config_file=dask_config_file,
                app=app,
                close_timeout=close_timeout,
                loop=loop,
                log_dir=log_dir
            )

        if app.cluster_type == Cluster.KUBE:
            return KubernetesCluster(
                dask_config_file=dask_config_file,
                app=app,
                close_timeout=close_timeout,
                loop=loop,
                log_dir=log_dir,
                end_event=end_event
            )

        raise JobardError('Cluster, cluster type not supported')

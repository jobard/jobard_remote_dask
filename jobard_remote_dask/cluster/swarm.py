"""Swarm Driver class."""
import asyncio
import json
import pathlib
import random
import re
import socket
import subprocess
import uuid
from typing import Tuple

import dask
import distributed
from filelock import FileLock

from jobard_remote_dask.cluster.base import ClusterBase
from jobard_remote_dask.core.exceptions import JobardClusterCriticalError, JobardError, JobardTimeoutError
from jobard_remote_dask.core.helpers.asynchronous import async_popen
from jobard_remote_dask.core.helpers.human import parse_human_size
from jobard_remote_dask.core.helpers.net import get_free_port, get_ip_address
from jobard_remote_dask.core.type.cluster import ClusterDaskConfig
from jobard_remote_dask.model.event.app import AppEvent
from jobard_remote_dask.model.swarm.container import Container
from jobard_remote_dask.model.swarm.host import Host


class SwarmCluster(ClusterBase):
    """Swarm Driver class."""

    # Swarm CPU Nano definition
    CPU_NANO: int = 1000000000

    # Swarm Docker ID validator
    CONTAINER_ID_REGEXP = re.compile(r'\w+')

    # Swarm Docker ID length
    CONTAINER_ID_LEN: int = 12

    # Popen (for docker command) timeout
    POPEN_TIMEOUT_SECONDS: int = 120

    # Check if the image is related to jobard (jobarded or jobard_worker_dask must be in the name)
    DETECT_JOBARD_IMAGES: Tuple[str] = ('jobarded', 'jobard_worker_dask')

    # Sleep between two check for containers readiness
    CHECK_CONTAINERS_READINESS_AFTER_SUBMIT_SLEEP_INTERVAL_SECONDS: int = 10

    # Exponential backoff algorithm to retry swarm containers creation
    # Keep in mind that the total time is determined by
    RETRY_CONTAINERS_SUBMIT_N: int = 16  # about 18 hours MAX

    # TCP Mapping Server global timeout
    TCP_MAPPING_SERVER_GLOBAL_TIMEOUT: int = 40

    # TCP Mapping Server encoding scheme
    TCP_MAPPING_SERVER_ENCODING_SCHEME: str = 'ascii'

    # Max lines to write to "cluster"'s job order directory, from a given container logs
    LOG_OUTPUT_MAX_LINES: int = 20000

    def __init__(
            self,
            dask_config_file: pathlib.Path,
            app: AppEvent,
            close_timeout: int,
            loop,
            log_dir: pathlib.Path,
            end_event: asyncio.Event
    ):
        """PBS Driver constructor."""

        # set the log dir
        self.log_dir = log_dir

        # auto-generated swarm service name
        self.service_name = None

        # interval simple tcp server to adjust dask-worker contact-address, regarding
        self.mapping_tcp = None

        # End event, set when the whole application must shut down
        self.end_event = end_event

        # started containers
        self.containers_started = {}

        # call super constructor
        super().__init__(dask_config_file=dask_config_file, app=app, close_timeout=close_timeout, loop=loop)
        self.cluster_dask_config = ClusterDaskConfig.SWARM
        super().set_dask_configuration()

    async def job_queue_check_container(self, worker):
        """
            Check the state of a running, valid, container

            Trigger a JobardError Exception, if there is a non-critical issue (one of the container is down).
            Trigger a JobardClusterCriticalError, if there is a critical issue, and the Dask app must be terminated.

            Return True, if the job is running.
            Return False, if the job is enqueued, but not running yet.
        """

        # ignore
        return True

    async def job_queue_check_container_historical_state(self, worker):
        """
            If the previous method can't get the state of the container (because it is finished),
            this method provide a way, to check its final state, with some kind of "history" command.

            Trigger a JobardClusterCriticalError, if there is a critical problem, and the Dask app must be terminated.
        """

        # ignore

    async def check_replicas(self):
        """Check replicas."""

        # check if the number of replicas of the docker service is equal to the expected one
        returncode, stdout, stderr = await async_popen(

            # docker service ls
            'docker',
            'service',
            'ls',

            # filter on the service name
            '--filter',
            'name=' + self.service_name,

            # return replicas only
            '--format',
            '{{.Replicas}}',

            timeout=self.POPEN_TIMEOUT_SECONDS,
            custom_logger=self._logger

        )

        if returncode != 0:
            self._logger.warn(stderr)
            raise JobardError('Cluster monitoring, unable to access service replicas information')

        # parse and check for replicas equality
        replicas = stdout.replace('\r', '').replace('\t', '').strip().split('/')

        self._logger.info('Cluster monitoring, up replicas : ' + replicas[0] + '/' + replicas[1])

        return 0 < int(replicas[1]) == int(replicas[0])

    async def monitor_cluster(self):
        """Monitor the cluster"""

        # override the entire monitoring system in case of swarm
        # there are no adaptive scaling with this Swarm Driver, so we just have to check if replicas are up (or not)
        ret = await self.check_replicas()
        if ret is False:
            raise JobardError('Cluster monitoring, at least one replicas has gone. error. terminating app..')

        # signal that at list a container is up
        self.set_one_container_up()

        # do scheduler health check
        self.common_scheduler_health_check(self.cluster.scheduler)

    async def get_available_nodes(self):
        """Get Swarm available nodes."""

        # get available swarm nodes
        returncode, stdout, stderr = await async_popen(
            'curl',
            '--unix-socket',
            '/var/run/docker.sock',
            'http://127.0.0.1/nodes',
            timeout=self.POPEN_TIMEOUT_SECONDS,
            custom_logger=self._logger
        )

        if returncode != 0:
            raise JobardError('Cluster, unable to check swarm nodes')

        try:
            ret = json.loads(stdout)

            # extract each node's hostname, and resources.
            available_swarm_nodes = list(
                [
                    Host(
                        hostname=x['Description']['Hostname'],
                        cpu_declared=x['Description']['Resources']['NanoCPUs'],
                        mem_declared=x['Description']['Resources']['MemoryBytes'],
                    )
                    # be sure the node is ready and active
                    for x in ret if x['Spec']['Availability'] == 'active' and x['Status']['State'] == 'ready'
                ]
            )

        except Exception:
            raise JobardError('Cluster, unable to parse swarm nodes')

        return available_swarm_nodes

    async def get_available_resources(self, swarm_nodes):
        """Get available resources on Swarm cluster."""

        all_containers = []

        # determine available resources
        for swarm_node in swarm_nodes:

            # list running containers
            returncode, stdout, stderr = await async_popen(
                'ssh',
                '-4',
                '-o',
                'StrictHostKeyChecking=no',
                swarm_node.hostname,
                'curl',
                '--unix-socket',
                '/var/run/docker.sock',
                'http://127.0.0.1/containers/json',
                timeout=self.POPEN_TIMEOUT_SECONDS,
                custom_logger=self._logger
            )
            if returncode != 0:
                raise JobardError('Cluster, unable to list containers')

            try:
                ret = json.loads(stdout)

                containers = list(
                    [
                        Container(
                            id=x['Id'],
                            # check if the Docker image is related to jobard
                            is_jobard_unit=self.is_jobard_image(x['Image']),
                            cpu_declared=0,
                            mem_declared=0
                        )
                        # ensure the container is running
                        for x in ret if x['State'] == 'running'
                    ]
                )

            except Exception:
                raise JobardError('Cluster, unable to parse swarm containers list')

            # get declared resources for each container
            command = [
                'ssh',
                '-4',
                '-o',
                'StrictHostKeyChecking=no',
                swarm_node.hostname,
                'curl',
                '--unix-socket',
                '/var/run/docker.sock']

            # Construct check command by container group
            group_size = dask.config.get('swarm.nb_container_per_swarm_node_check')
            list_cmd = [
                containers[i:i + group_size] for i in range(0, len(containers), group_size)]
            for cmd in list_cmd:
                cmd_urls = []
                for container in cmd:
                    cmd_urls.append('http://127.0.0.1/containers/' + container.id + '/json')
                returncode, stdout, stderr = await async_popen(
                            *(command + cmd_urls),
                            timeout=self.POPEN_TIMEOUT_SECONDS,
                            custom_logger=self._logger
                        )

                if returncode != 0:
                    raise JobardError('Cluster, unable to list containers')

                try:
                    results = stdout.splitlines()
                    for res in results:
                        ret = json.loads(res)
                        if 'message' in ret:
                            raise JobardError('Cluster, error message returned, unable to parse inspect result')
                        container_id = ret['Id']
                        result = list(filter(lambda x: x.id == container_id, containers))
                        result[0].cpu_declared = ret['HostConfig']['NanoCpus']
                        result[0].mem_declared = ret['HostConfig']['Memory']

                        # Log a warning if the memory limitation of the container is not set
                        if ret['HostConfig']['MemoryReservation'] == 0:
                            self._logger.warn(
                                'Cluster, CONTAINER : ' + ret['Name'] + ' ID : ' + ret['Id']
                                + ' has no memory limitation, so it can lead to memory overflow'
                            )

                except Exception:
                    raise JobardError('Cluster, unable to parse inspect result')

            all_containers.extend(containers)

        return all_containers

    async def check_node_resources(self) -> bool:
        """Check nodes resources availability for the given app."""

        # get cluster limits for jobard from config
        cluster_max_vcpu_for_jobard = dask.config.get('swarm.cluster_max_vcpu_for_jobard')
        cluster_max_memory_for_jobard = dask.config.get('swarm.cluster_max_memory_for_jobard')

        # determine cluster resources allocated for jobard
        cluster_max_cpu_nanos_for_jobard = cluster_max_vcpu_for_jobard * self.CPU_NANO
        cluster_max_memory_for_jobard = parse_human_size(cluster_max_memory_for_jobard)

        # determine requested resources
        cores_requested = self.app.worker_cores * self.app.n_workers
        mem_requested = parse_human_size(self.app.worker_memory) * self.app.n_workers

        self._logger.info('Cluster, REQUESTED RESOURCES. ' + str(cores_requested) + ' CORES. ' + str(
            mem_requested // (1024 ** 2)
        ) + ' MEMORY MiB.')

        # get available swarm nodes info
        available_swarm_nodes = await self.get_available_nodes()

        # get total available resources
        cluster_total_memory = sum(list([node.mem_declared for node in available_swarm_nodes]))
        cluster_total_cpu = sum(list([node.cpu_declared for node in available_swarm_nodes]))

        # log the nodes available
        self._logger.info(
            'Cluster, TOTAL CLUSTER NODES AVAILABLE : ' + str(len(available_swarm_nodes))
        )

        # log the total cluster resources
        self._logger.info(
            'Cluster, TOTAL CLUSTER RESOURCES. ' +
            str(cluster_total_cpu // self.CPU_NANO) + ' CORES. ' +
            str(cluster_total_memory // (1024 ** 2)) + ' MEMORY MiB.'
        )

        # log the total cluster allocated for jobard
        self._logger.info(
            'Cluster, TOTAL CLUSTER RESOURCES FOR JOBARD. ' +
            str(cluster_max_cpu_nanos_for_jobard // self.CPU_NANO) + ' CORES. ' +
            str(cluster_max_memory_for_jobard // (1024 ** 2)) + ' MEMORY MiB.'
        )

        # if requested resources exceed cluster resources, send a critical error.
        if mem_requested > cluster_total_memory:
            self._logger.info('Cluster, REQUESTED MEMORY EXCEED TOTAL MEMORY')
            raise JobardClusterCriticalError(
                'Cluster, critical error :  REQUESTED MEMORY EXCEED TOTAL MEMORY'
            )

        if cores_requested * self.CPU_NANO > cluster_total_cpu:
            self._logger.info('Cluster, REQUESTED CORES EXCEED TOTAL CORES')
            raise JobardClusterCriticalError(
                'Cluster, critical error :  REQUESTED CORES EXCEED TOTAL CORES'
            )

        # if requested resources exceed max cluster resources allocated for Jobard, send a critical error.
        if mem_requested > cluster_max_memory_for_jobard:
            self._logger.info('Cluster, REQUESTED MEMORY EXCEED JOBARD CLUSTER ALLOCATED MEMORY')
            raise JobardClusterCriticalError(
                'Cluster, critical error :  REQUESTED MEMORY EXCEED JOBARD CLUSTER ALLOCATED MEMORY'
            )

        if cores_requested * self.CPU_NANO > cluster_max_cpu_nanos_for_jobard:
            self._logger.info('Cluster, REQUESTED CORES EXCEED JOBARD CLUSTER ALLOCATED CORES')
            raise JobardClusterCriticalError(
                'Cluster, critical error :  REQUESTED CORES EXCEED JOBARD CLUSTER ALLOCATED CORES'
            )


        # if max cluster resources allocated for Jobard exceed cluster resources, send a critical error.
        if cluster_max_memory_for_jobard > cluster_total_memory:
            self._logger.info('Cluster, JOBARD CLUSTER ALLOCATED MEMORY EXCEED TOTAL CLUSTER MEMORY')
            raise JobardClusterCriticalError(
                'Cluster, critical error :  JOBARD CLUSTER ALLOCATED MEMORY EXCEED TOTAL CLUSTER MEMORY'
            )

        if cluster_max_cpu_nanos_for_jobard > cluster_total_cpu:
            self._logger.info('Cluster, JOBARD CLUSTER ALLOCATED CORES EXCEED TOTAL CLUSTER CORES')
            raise JobardClusterCriticalError(
                'Cluster, critical error :  JOBARD CLUSTER ALLOCATED CORES EXCEED TOTAL CLUSTER CORES'
            )

        # get available resources
        all_containers = await self.get_available_resources(available_swarm_nodes)

        # get the cluster currently used resources
        cluster_current_memory_used = sum(list([container.mem_declared for container in all_containers]))
        cluster_current_cpu_nanos_used = sum(list([container.cpu_declared for container in all_containers]))

        # get the cluster currently used resources for "jobarded" images
        cluster_current_cpu_nanos_used_for_jobard = sum(list([
            container.cpu_declared for container in all_containers if container.is_jobard_unit is True]))
        cluster_current_memory_used_for_jobard = sum(list([
            container.mem_declared for container in all_containers if container.is_jobard_unit is True]))

        # log the cluster currently used resources
        self._logger.info('Cluster, USED CLUSTER RESOURCES. ' +
                          str(cluster_current_cpu_nanos_used // self.CPU_NANO) + ' CORES. ' +
                          str(cluster_current_memory_used // (1024 ** 2)) + ' MEMORY MiB.')

        # log the "jobarded" currently used resources
        self._logger.info('Cluster, USED CLUSTER RESOURCES FOR JOBARD. ' +
                          str(cluster_current_cpu_nanos_used_for_jobard // self.CPU_NANO) + ' CORES. ' +
                          str(cluster_current_memory_used_for_jobard // (1024 ** 2)) + ' MEMORY MiB.')

        # check if cluster resources are enough
        if cluster_current_memory_used + mem_requested > cluster_total_memory:
            self._logger.info('Cluster, GLOBAL CLUSTER HAS NOT ENOUGH AVAILABLE MEMORY')
            return False

        if cluster_current_cpu_nanos_used + cores_requested * self.CPU_NANO > cluster_total_cpu:
            self._logger.info('Cluster, GLOBAL CLUSTER HAS NOT ENOUGH AVAILABLE CORES')
            return False

        # check if dedicated resources for jobard are enough
        if cluster_current_memory_used_for_jobard + mem_requested > cluster_max_memory_for_jobard:
            self._logger.info('Cluster, JOBARD CLUSTER HAS NOT ENOUGH AVAILABLE MEMORY')
            return False

        cores = cluster_current_cpu_nanos_used_for_jobard + cores_requested * self.CPU_NANO
        if cores > cluster_max_cpu_nanos_for_jobard:
            self._logger.info('Cluster, JOBARD CLUSTER HAS NOT ENOUGH AVAILABLE CORES')
            return False

        return True

    async def workers_submit(self, scheduler_address, mapping_tcp_host, mapping_tcp_port) -> bool:
        """Submit and scale the APP."""

        # Cleanup existing containers
        self.cleanup_containers()

        # auto-generated swarm service name
        self.service_name = 'jobarded_' + uuid.uuid4().hex
        self._logger.info(f'Cluster, setting the service name : {self.service_name}')

        #
        # Docker service configuration
        #

        args = [

            # docker service create
            'docker',
            'service',
            'create',

            # use replicated mode
            '--mode',
            'replicated',

            # non-blocking call
            '--detach',

            # number of replicas = number of workers
            '--replicas',
            str(self.app.n_workers),

            # CPU limit
            '--limit-cpu',
            str(self.app.worker_cores),

            # Memory limit
            '--limit-memory',
            str(parse_human_size(self.app.worker_memory)) + 'B',

            # Schedule on a node only if it can give that resources
            '--reserve-cpu',
            str(self.app.worker_cores),
            '--reserve-memory',
            str(parse_human_size(self.app.worker_memory)) + 'B',

            # Prevent node restarting the worker/container
            '--restart-max-attempts',
            str(0),
            '--restart-condition',
            'none',

            # Publish Dask Worker TCP port 9000 on a free and random container's host port
            '--publish',
            'target=9000,mode=host',
        ]

        #
        # Push Docker image-specific service args (like mount point)
        #
        for key, (source, destination) in self.app.docker_mount_points.items():
            args.extend(['--mount', f'type=bind,source={source},destination={destination}'])

        # If defined, set the Unix user from the app event
        if self.app.unix_uid:
            args.extend(['--user', f'{self.app.unix_uid}'])

        #
        # Set the service and image name
        #

        args.extend([

            # set the service name
            '--name',
            self.service_name,

            # set the full image name
            self.app.image,

        ])

        #
        # Set the dask-worker args
        #
        args.extend([

            # set the binary
            '/venv/jobard_worker_dask/bin/dask-worker',

            # set the scheduler address
            scheduler_address,

            # set the preload script (redirect dask-worker's contact-address to swarm host directly)
            '--preload',
            '/venv/jobard_worker_dask/lib/python3.9/site-packages/jobard_worker_dask/swarm.py',

            # local directory
            '--local-directory',
            '/tmp/',

            # avoid any threads
            '--nthreads',
            '1',

            # automatically leave room for system OS overhead
            '--memory-limit',
            'auto',

            # listen inside the container on port 9000
            '--listen-address',
            'tcp://0.0.0.0:9000',

            # set the mapping tcp service contact point, used by the preload script
            '--mapping-tcp-host',
            str(mapping_tcp_host),
            '--mapping-tcp-port',
            str(mapping_tcp_port),

            # disable nanny
            '--no-nanny',

            # disable dashboard
            '--no-dashboard',

            # set death timeout (avoid workers to stick around needlessly if their scheduler goes away)
            '--death-timeout',
            str(self.app.death_timeout),

            # set resources
            '--resources',
            'CPU=' + str(self.app.worker_cores) + ',MEM=' + str(parse_human_size(self.app.worker_memory)),
        ])

        self._logger.info(' '.join(args))

        # run the docker swarm service creation
        returncode, stdout, stderr = await async_popen(
            *args,
            timeout=self.POPEN_TIMEOUT_SECONDS,
            custom_logger=self._logger
        )

        self._logger.info('Cluster, docker service create stdout: ' + stdout)
        self._logger.info('Cluster, docker service create stderr: ' + stderr)

        if returncode != 0 and 'already exists' not in stdout and 'already exists' not in stderr:
            raise JobardError('Cluster, unable to submit the docker service')

        replicas_ready = False

        # wait until the containers are up and running AND the whole application must not terminate ASAP
        while not self.end_event.is_set():

            services_ready = False

            if replicas_ready is False:

                ret = await self.check_replicas()
                if ret is True:
                    self._logger.info('Cluster, REPLICAS READY !')
                    replicas_ready = True

            # check if the services are ready
            returncode, stdout, stderr = await async_popen(

                # docker service ps
                'docker',
                'service',
                'ps',

                # restrict on the service name
                self.service_name,

                # avoid truncating output to ease parsing
                '--no-trunc',

                # return only the state of the containers
                '--format',
                '{{.CurrentState}}|{{.Error}}',

                timeout=self.POPEN_TIMEOUT_SECONDS,
                custom_logger=self._logger

            )
            if returncode != 0:
                self._logger.warn(stderr)
                raise JobardError('Cluster, unable to access services state information')

            lines = stdout.replace('\r', '').replace('\t', '').lower().strip().split('\n')
            self._logger.debug(lines)
            states = list(map(lambda x: '' if '|' not in x else x.split('|')[0], lines))
            self._logger.debug(states)
            errors = ';'.join(list(set(list(map(lambda x: '' if '|' not in x else x.split('|')[1], lines)))))
            self._logger.debug(errors)

            i = 0
            for state in states:

                # if the state seems weird, shutdown immediately the whole Dask application
                if 'failed' in state:
                    raise JobardClusterCriticalError(
                        'Cluster, critical error, state of one of the containers is FAILED, while initializing :' +
                        errors
                    )
                if 'shutdown' in state:
                    raise JobardClusterCriticalError(
                        'Cluster, critical error, state of one of the containers is SHUTDOWN, while initializing :' +
                        errors
                    )
                if 'rejected' in state:
                    raise JobardClusterCriticalError(
                        'Cluster, critical error, state of one of the containers is REJECTED, while initializing :' +
                        errors
                    )
                if 'complete' in state:
                    raise JobardClusterCriticalError(
                        'Cluster, critical error, state of one of the containers is COMPLETE, while initializing :' +
                        errors
                    )
                if 'orphaned' in state:
                    raise JobardClusterCriticalError(
                        'Cluster, critical error, state of one of the containers is ORPHANED, while initializing :' +
                        errors
                    )
                if 'remove' in state:
                    raise JobardClusterCriticalError(
                        'Cluster, critical error, state of one of the containers is REMOVED, while initializing :' +
                        errors
                    )

                # if the state is RUNNING, increment the number of replicas in RUNNING state
                if 'running' in state:
                    i = i + 1

                # for other state, we will wait, until they reach the RUNNING state

            # check if all containers are running or not
            if len(states) == i:
                self._logger.info('Cluster, ALL CONTAINERS ARE UP AND RUNNING')
                services_ready = True

            # return if the two conditions (replicas and services readiness) are met
            if services_ready is True and replicas_ready is True:
                self._logger.info('Cluster, SERVICES READY!')
                break

            # do wait before the next check
            await asyncio.sleep(self.CHECK_CONTAINERS_READINESS_AFTER_SUBMIT_SLEEP_INTERVAL_SECONDS)

        return True

    async def mapping_tcp_handle(self, reader, writer):
        """Handle a new connection to the TCP server"""

        try:

            # if the service name is defined
            if self.service_name is not None:

                # read the Docker container ID
                container_id_raw = await asyncio.wait_for(
                    reader.read(self.CONTAINER_ID_LEN),
                    timeout=self.TCP_MAPPING_SERVER_GLOBAL_TIMEOUT
                )

                container_id = container_id_raw.decode(self.TCP_MAPPING_SERVER_ENCODING_SCHEME).strip()

                self._logger.info('Cluster, asking for container ID ' + container_id + ' mapping information')

                # if the container id is valid (security purpose)
                if re.fullmatch(self.CONTAINER_ID_REGEXP, container_id):

                    # TODO : do some caching, to speedup when numerous of nodes

                    # get involved nodes
                    returncode, stdout, stderr = await async_popen(

                        # docker service ps
                        'docker',
                        'service',
                        'ps',

                        # set service name
                        self.service_name,

                        # avoid truncating output, to ease parsing
                        '--no-trunc',

                        # return only the nodes that host the containers
                        '--format',
                        '{{.Node}}',

                        timeout=self.POPEN_TIMEOUT_SECONDS,
                        custom_logger=self._logger

                    )

                    if returncode != 0:
                        self._logger.warn(
                            'Cluster, unable to retrieve container information, exit code = {0}'.format(returncode)
                        )
                        self._logger.warn(stderr)
                    else:

                        res = ''

                        # extract the list of nodes implied in the computation
                        nodes = stdout.replace('\r', '').replace('\t', '').strip().split('\n')

                        # for each nodes..
                        for node in nodes:

                            self._logger.info('Cluster, searching ' + container_id + ' on node ' + node + ' ...')

                            # search and get containers hosts
                            returncode, stdout, stderr = await async_popen(

                                # ipv4 ssh connection, without strict host key check
                                'ssh',
                                '-4',
                                '-o',
                                'StrictHostKeyChecking=no',
                                node,

                                # docker ps
                                'docker',
                                'ps',
                                # avoid truncating output, to ease parsing
                                '--no-trunc',

                                # return only the ports
                                '--format',
                                '"{{.Ports}}"',

                                # filter on the container ID
                                '--filter',
                                'id=' + container_id,

                                timeout=self.POPEN_TIMEOUT_SECONDS,
                                custom_logger=self._logger

                            )

                            if returncode != 0:
                                self._logger.warn(
                                    'Cluster, unable to retrieve container information on node ' + node
                                    + ', exit code = ' + str(returncode))
                                self._logger.warn(stderr)
                            else:
                                stdout = stdout \
                                    .replace('\r', '') \
                                    .replace('\t', '') \
                                    .replace('\n', '').strip()
                                if stdout != '':
                                    res = socket.gethostbyname(node) + '|' + stdout
                                    self.containers_started[container_id] = node
                                    break

                        self._logger.info('Cluster, responding back ' + res)
                        writer.write(res.encode(self.TCP_MAPPING_SERVER_ENCODING_SCHEME))
                else:
                    self._logger.warn('Cluster, unable to validate container ID')
        finally:

            # tcp writer drain
            try:
                await writer.drain()
            except Exception as e:
                self._logger.warn(e)

            # graceful cleanup
            if writer is not None:
                if not writer.is_closing():
                    try:
                        writer.close()
                        await asyncio.wait_for(writer.wait_closed(), timeout=self.TCP_MAPPING_SERVER_GLOBAL_TIMEOUT)
                    except Exception as e:
                        self._logger.warn(e)

    def check_constraints(self):
        """Validate app compatibility with Swarm."""

        # prevent dynamic scaling (not supported)
        if self.app.n_workers is None:
            raise JobardError('Cluster, dynamic/adapt scaling not supported yet with swarm driver')

        # prevent app without any Docker image set
        if self.app.image is None:
            raise JobardError('Cluster, Docker image not set for this app')

    async def start_cluster(self):
        """Start the cluster."""

        # first, check constraints before trying to run any container
        self.check_constraints()

        # find free port for TCP mapping server
        mapping_tcp_host = get_ip_address(dask.config.get('swarm.public_interface'))
        mapping_tcp_port = get_free_port(mapping_tcp_host)

        # start the internal mapping tcp server
        self.mapping_tcp = await asyncio.wait_for(
            asyncio.start_server(self.mapping_tcp_handle, host=mapping_tcp_host, port=mapping_tcp_port),
            timeout=self.TCP_MAPPING_SERVER_GLOBAL_TIMEOUT
        )

        self._logger.info(
            'Cluster, internal tcp server running on tcp://' + mapping_tcp_host + ':' + str(mapping_tcp_port)
        )

        # build cluster arguments
        arguments = {

            # log directory for both workers output and jobs output
            'log_directory': self.log_dir.as_posix() + pathlib.os.sep,

            # async usage
            'asynchronous': True,

            # always use processes
            'processes': True,

            # workers will be added later
            'n_workers': 0,

            # insecure protocol supported only
            'protocol': 'tcp://',

            # set network interface
            'interface': dask.config.get('swarm.public_interface'),

            # set random ports for dashboard
            'dashboard_address': None,
        }

        # create the cluster
        self.cluster = await distributed.LocalCluster(**arguments)

        # log some cluster info
        self._logger.info(self.cluster.scheduler.address)
        self._logger.info(self.cluster.dashboard_link)

        # get the scheduler address
        scheduler_address = self.cluster.scheduler.address

        # wait for available resources in another process
        x = 0
        swarm_lock = FileLock(dask.config.get('swarm.lock'))
        while not self.end_event.is_set():
            try:
                ret_check_resources = False
                ret_worker_submit = False

                # global lock in order to avoid resources contention
                with swarm_lock:
                    # first try to check for available resources
                    try:
                        ret_check_resources = await self.check_node_resources()
                    except JobardClusterCriticalError as jcce:
                        self._logger.error(
                            'Cluster, critical exception occured while trying to acquire cluster resources'
                        )
                        raise jcce
                    except JobardTimeoutError as jte:
                        self._logger.warn(
                            'Cluster, non-fatal timeout exception occured while trying to acquire cluster resources'
                        )
                        self._logger.warn(jte)
                    except JobardError as je:
                        self._logger.warn(
                            'Cluster, non-fatal non-timeout exception occured while trying to acquire cluster resources'
                        )
                        self._logger.warn(je)
                    except Exception as e:
                        self._logger.error(
                            'Cluster, fatal exception while trying to acquire cluster resources'
                        )
                        raise e

                    if ret_check_resources is True:
                        cleanup_needed = True
                        try:
                            ret_worker_submit = await self.workers_submit(scheduler_address, mapping_tcp_host, mapping_tcp_port)
                            cleanup_needed = False
                            break
                        except JobardClusterCriticalError as jcce:
                            self._logger.error(
                                'Cluster, critical exception occured while submitting Swarm service'
                            )
                            raise jcce
                        except JobardTimeoutError as jte:
                            self._logger.warn(
                                'Cluster, non-fatal timeout exception occured while submitting Swarm service'
                            )
                            self._logger.warn(jte)
                        except JobardError as je:
                            self._logger.warn(
                                'Cluster, non-fatal non-timeout exception occured while submitting Swarm service'
                            )
                            self._logger.warn(je)
                        except Exception as e:
                            self._logger.error(
                                'Cluster, fatal exception while submitting Swarm service'
                            )
                            raise e
                        finally:
                            if cleanup_needed is True:
                                self.cleanup_containers()
            except Exception as e:
                self._logger.error(
                    'Cluster, fatal exception with Swarm Lock'
                )
                raise e

            if ret_check_resources is False or ret_worker_submit is False:
                if x == self.RETRY_CONTAINERS_SUBMIT_N:
                    raise JobardError(
                        'Cluster, too many tries before having resources from swarm'
                    )
                else:
                    # exponential backoff
                    sleep = 2 ** x + random.uniform(0, 1)
                    self._logger.info(
                        'Cluster, WAITING FOR :' + str(sleep) + 's'
                    )
                    await asyncio.sleep(sleep)
                    x += 1

    async def backup_logs(self):
        """Backup logs to a different directory."""

        self._logger.info('Cluster, writing logs to job order\'s "cluster" sub-directory')

        try:

            # for each containers
            for container_id in self.containers_started:

                # get the node hostname
                node = self.containers_started[container_id]

                self._logger.info('Cluster, writing logs for ' + container_id + ' / ' + node + ' ..')

                # write if not already exists
                stderr_p = self.log_dir.joinpath(container_id + '_stderr.log')
                stdout_p = self.log_dir.joinpath(container_id + '_stdout.log')
                if not stderr_p.exists() and not stdout_p.exists():

                    # dump logs to job order "cluster" sub-directory
                    returncode, stdout, stderr = await async_popen(

                        # ipv4 ssh connection, without strict host key check
                        'ssh',
                        '-4',
                        '-o',
                        'StrictHostKeyChecking=no',
                        node,

                        # docker logs
                        'docker',
                        'logs',

                        # show extra details provided to logs
                        '--details',

                        # show timestamps
                        '--timestamps',

                        # restrict max lines
                        '--tail',
                        str(self.LOG_OUTPUT_MAX_LINES),

                        # restrict on the given container ID
                        container_id,

                        timeout=self.POPEN_TIMEOUT_SECONDS,
                        custom_logger=self._logger

                    )

                    if returncode != 0:
                        self._logger.warn(
                            'Cluster, unable to retrieve container ' + container_id + ' logs on node ' + node
                            + ', exit code = ' + str(returncode))
                        self._logger.warn(stderr)

                    stdout_p.write_text(stdout)
                    stderr_p.write_text(stderr)

        except Exception as error:
            self._logger.warn('Cluster, error while doing backup of containers logs')
            self._logger.warn(str(error))

    def cleanup_containers(self):
        """cleanup/terminate the swarm service."""

        self._logger.info('Cluster, dropping Swarm service')

        if self.service_name is not None:
            try:
                subprocess.run(
                    [
                        'docker',
                        'service',
                        'rm',
                        self.service_name
                    ],
                    shell=False,
                    timeout=self.POPEN_TIMEOUT_SECONDS,
                    universal_newlines=True
                )
            except Exception as e:
                self._logger.warn(e)
            finally:
                self.service_name = None

    async def cleanup_mapping_tcp(self):
        """cleanup tcp mapping server."""

        if self.mapping_tcp is not None:
            try:
                self.mapping_tcp.close()
                await asyncio.wait_for(self.mapping_tcp.wait_closed(), timeout=self.TCP_MAPPING_SERVER_GLOBAL_TIMEOUT)
            except Exception as e:
                self._logger.warn(e)
            finally:
                self.mapping_tcp = None

    async def close_cluster(self):
        """Stop the cluster."""

        await self.backup_logs()

        self.cleanup_containers()

        await self.cleanup_mapping_tcp()

        await super().close_cluster()

    @staticmethod
    def is_jobard_image(image: str) -> bool:
        """
        Test if docker image is a Jobard image
        Args:
            image: The image to test.
        Returns:
            True for a Jobard image, False else.
        """
        return len([job_image for job_image in SwarmCluster.DETECT_JOBARD_IMAGES if job_image in image]) > 0

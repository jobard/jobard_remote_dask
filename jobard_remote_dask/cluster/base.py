"""Cluster interface class."""
import logging
import os
import pathlib
from abc import ABC, abstractmethod
from datetime import datetime

import dask
from distributed.metrics import time

from jobard_remote_dask.core.exceptions import JobardClusterCriticalError, JobardError, JobardTimeoutError
from jobard_remote_dask.core.helpers.walltime import seconds_to_walltime, walltime_to_seconds
from jobard_remote_dask.model.event.app import AppEvent


class ClusterBase(ABC):
    """Cluster interface class."""

    DASK_LIFETIME_MARGIN: int = walltime_to_seconds('00:05:00')
    DASK_LIFETIME_STAGGER_MARGIN: int = walltime_to_seconds('00:01:00')

    def __init__(self, app: AppEvent, dask_config_file: pathlib.Path, close_timeout: int, loop):
        """Cluster constructor."""

        # Get the logger
        self._logger = logging.getLogger('jobard_remote_dask')

        # Dask Config file
        self.dask_config_file = dask_config_file

        # Cluster Dask configuration
        self.cluster_dask_config = None

        # Underlying cluster object
        self.cluster = None

        # Object that describe a common distributed application
        self.app = app

        # Timeout before terminating abruptly the cluster, in case of shutdown
        self.close_timeout = close_timeout

        # The current thread asyncio loop (deprecated from Python 3.10)
        self.loop = loop

        # Capture the time when the first container (and not necessary the dask-worker inside) is running
        self.one_container_up_time = None

        # Down containers (worker addresses) that we don't want to check again the running states
        self.down_containers = []

        # History containers (worker addresses) that we don't want to check again the historical state
        self.historical_containers = []

        # If one task is done (from the client point of view), we can start to analyze dask-workers state
        self.one_task_done = False

    def ensure_dask_config_file(self, destination=None, no_error=False, comment=True):
        """
        Copy file to default location if it does not already exist
        This tries to move a default configuration file to a default location if
        it does not already exist.  It also comments out that file by default.
        Args
        destination : string, directory
            Destination directory.
        no_error : bool, False by default
            If True, just warn for the creation of a default configuration file.
            If False, sends and error.
        comment : bool, True by default
            Whether or not to comment out the config file when copying.
        """

        # destination is a file and already exists, never overwrite
        if os.path.isfile(destination):
            return

        # If destination is not an existing file, interpret as a directory,
        # use the source basename as the filename
        directory = os.path.expanduser(destination)
        source = os.path.join(os.path.dirname(__file__), self.cluster_dask_config)
        destination = os.path.expanduser(os.path.join(directory, os.path.basename(source)))

        try:
            if not os.path.exists(destination):
                os.makedirs(directory, exist_ok=True)

                # Atomically create destination.  Parallel testing discovered
                # a race condition where a process can be busy creating the
                # destination while another process reads an empty config file.
                tmp = "%s.tmp.%d" % (destination, os.getpid())
                with open(source) as f:
                    lines = list(f)

                if comment:
                    lines = [
                        "# " + line if line.strip() and not line.startswith("#") else line
                        for line in lines
                    ]

                with open(tmp, "w") as f:
                    f.write("".join(lines))

                try:
                    os.rename(tmp, destination)
                except OSError:
                    os.remove(tmp)

                # For local cluster, just warns the user a default configuration file has been created.
                # because the default configuration file is not mandatory for local cluster.
                msg = f'Cluster, no configuration file found in : {directory} for this cluster.\n' \
                      f'A default configuration file {self.cluster_dask_config} is created.\n'
                if no_error:
                    self._logger.warning(msg)
                # For other clusters, raises an error and warns the user to update the default configuration.
                else:
                    msg2 = f'This file MUST BE updated with the cluster configuration.'
                    self._logger.error(msg+msg2)
                    raise JobardClusterCriticalError(msg+msg2)
            else:
                # For non-local clusters, raises an error and warns the user to update the default configuration.
                # If a configuration file already exists, check if it is the same as the default configuration.
                if not no_error:
                    with open(source, 'r') as file1:
                        with open(destination, 'r') as file2:
                            if file1.read() == file2.read():
                                msg = f'Cluster, the configuration file found in : {destination} ' \
                                      'MUST BE updated with the cluster configuration.'
                                self._logger.error(msg)
                                raise JobardClusterCriticalError(msg)
        except OSError:
            pass

    def set_dask_configuration(self, no_error=False):
        """
        Set the Dask configuration.
        Args:
            no_error : bool, False by default
            If True, just warn for the creation of a default configuration file.
            If False, sends and error.
        """
        # Ensure the is a Dask configuration file for the cluster
        self.ensure_dask_config_file(destination=self.dask_config_file, no_error=no_error, comment=False)

        # Merge the Dask configuration with user one
        config = dask.config.merge(
            dask.config.config,
            dask.config.collect(paths=[os.path.expanduser(self.dask_config_file)])
        )

        # Add some extra Dask config to limitate memory leaks
        extra_config = {'distributed': {
            'admin': {
                'log-length': 1,
                'low-level-log-length': 1,
                'max-error-length': 1,
                'system-monitor': {
                    'interval': '60000ms'
                },
                'tick': {
                    'cycle': '60s'
                }
            },
            'dashboard': {
                'graph-max-items': 1
            },
            'diagnostics': {
                'computations': {
                    'max-history': 1
                },
                'erred-tasks': {
                    'max-history': 1
                }
            },
            'scheduler': {
                'dashboard': {
                    'status': {'task-stream-length': 1},
                    'tasks': {'task-stream-length': 1},
                    'events-cleanup-delay': '10s',
                },
                'transition-log-length': 1,
                'events-log-length': 1,
            }
        }
        }

        dask.config.set(dask.config.merge(
            config,
            extra_config
        ))


    def lifetime(self):
        """Set Dask Worker lifetime parameters"""

        self._logger.info(
            'Cluster, TOTAL APP LIFETIME : ' + seconds_to_walltime(
                walltime_to_seconds(self.app.walltime) + self.app.application_processor_timeout
            ) + ' ( application_processor_timeout ' + str(self.app.application_processor_timeout) +
            's + container wall time ' + str(walltime_to_seconds(self.app.walltime)) + 's )'
        )

        lifetime = max(
            walltime_to_seconds(self.app.walltime) - self.DASK_LIFETIME_MARGIN,
            self.DASK_LIFETIME_MARGIN
        )

        self._logger.info(
            'Cluster, CONTAINER WALLTIME : ' + self.app.walltime
        )

        self._logger.info(
            'Cluster, BE AWARE THAT EVERY DASK WORKER INSIDE A CONTAINER HAS A LIFETIME SET TO (ABOUT) : ' +
            seconds_to_walltime(lifetime - self.DASK_LIFETIME_STAGGER_MARGIN)
        )

        # TODO : calculate dynamically
        return [

            # Workers will be properly shut down before the scheduling system kills them.
            #
            # The situation arise when you don't have a lot of room on your platform and have only a few
            # workers at a time (less than what you were hoping for when using scale or adapt).
            #
            # These workers will be killed (and others started) before your workload ends.
            # (thus terminating abruptly this Dask App)
            '--lifetime',
            str(lifetime) + 's',

            '--lifetime-stagger',
            str(self.DASK_LIFETIME_STAGGER_MARGIN) + 's',
        ]

    async def monitor_cluster(self):
        """Monitor the cluster and trigger Jobard exception in case of critical failure detection."""

        # check containers
        containers_running_count = await self.job_queue_check_containers()

        # check Dask-Workers
        if self.one_task_done is True or containers_running_count > 0 or self.one_container_up_time is not None:
            self.common_scheduler_health_check(self.cluster.scheduler)

    def set_one_task_done(self):
        self.one_task_done = True

    def set_one_container_up(self):
        if self.one_container_up_time is None:
            self.one_container_up_time = int(time())

    @abstractmethod
    async def start_cluster(self):
        """Start the cluster."""

    async def close_cluster(self):
        """Close the cluster."""

        # Terminate the cluster
        if self.cluster is not None:
            await self.cluster.close(timeout=self.close_timeout)

    @abstractmethod
    async def job_queue_check_container(self, worker):
        """
            Check the state of a running, valid, container

            Trigger a JobardError Exception, if there is a non-critical issue (one of the container is down).
            Trigger a JobardClusterCriticalError, if there is a critical issue, and the Dask app must be terminated.

            Return True, if the job is running.
            Return False, if the job is enqueued, but not running yet.
        """

    @abstractmethod
    async def job_queue_check_container_historical_state(self, worker):
        """
            If the previous method can't get the state of the container (because it is finished),
            this method provide a way, to check its final state, with some kind of "history" command.

            Trigger a JobardClusterCriticalError, if there is a critical problem, and the Dask app must be terminated.
        """

    async def job_queue_check_containers(self):
        """Job Queue Common containers-check Routine."""

        # check the submitted workers (jobs/containers) from Dask-JobQueue
        containers_submitted_len = len(self.cluster.workers)

        # workers (jobs/containers) are not yet submitted
        if containers_submitted_len == 0:
            self._logger.info(
                'Cluster monitoring, waiting for at least one worker (job) submitted to underlying scheduler..'
            )
            return 0

        # delegate the worker (job/container) state check to the underlying concrete cluster object
        containers_running = 0
        containers_in_queue = 0
        containers_down = 0

        n_workers_max = 0 if self.app.n_max_workers is None else self.app.n_max_workers
        expected_containers_count = max(self.app.n_workers, n_workers_max)

        # for each container
        # avoid exception by copying the array
        #    for i in self.cluster.workers:
        #      RuntimeError: dictionary changed size during iteration
        cluster_workers = self.cluster.workers.copy()
        for i in cluster_workers:
            worker = cluster_workers[i]
            self._logger.info('Cluster monitoring, N:' + str(worker.name) + ' J:' + str(worker.job_id) + ', checking..')
            # avoid checking multiples times a container that is marked as down
            if str(worker.name) not in self.down_containers:
                try:
                    # check a given worker's container. this method :
                    #  - trigger an exception if there is a critical problem with the container (not running, etc.)
                    #  - return True, if the container is running.
                    #  - return False, if the container is enqueued, but not running yet.
                    is_running = await self.job_queue_check_container(worker)
                    # if the container is running
                    if is_running is True:
                        # container marked as up
                        containers_running += 1
                        # signal that at least one container is alive
                        self.set_one_container_up()
                        self._logger.info(
                            'Cluster monitoring, N:' + str(worker.name) + ' J:' + str(worker.job_id) +
                            ', marked as RUNNING.'
                        )
                    else:
                        # container marked as in queue
                        containers_in_queue += 1
                        self._logger.info(
                            'Cluster monitoring, N:' + str(worker.name) + ' J:' + str(worker.job_id) +
                            ', marked as IN QUEUE.'
                        )
                except JobardClusterCriticalError as je:
                    # in case of critical exception, we propagate it immediately, as a generic Jobard Error
                    raise JobardError(str(je))
                except JobardTimeoutError:
                    # a timeout occured. we'll retry later
                    self._logger.info(
                        'Cluster monitoring, timeout occured, while monitoring container. '
                        'container NOT marked as down (marked as RUNNING). '
                        'will retry later, ..'
                    )
                    # container marked as running
                    containers_running += 1
                    self._logger.info(
                        'Cluster monitoring, N:' + str(worker.name) + ' J:' + str(worker.job_id) +
                        ', marked as RUNNING.'
                    )
                except JobardError as je:
                    # in case of non-timeout non-critical exception, we mark the container as down.
                    # we do not try to shut down the whole Dask application at this point, because a container
                    # might have finished gracefully, within its wall time
                    self._logger.info(
                        'Cluster monitoring, '
                        'non-timeout non-critical problem occured,'
                        ' while monitoring container. '
                        'container MARKED as DOWN. '
                    )
                    self.down_containers.append(str(worker.name))
                    self._logger.info(str(je))
                    # container marked as down
                    containers_down += 1
                    self._logger.info(
                        'Cluster monitoring, N:' + str(worker.name) + ' J:' + str(worker.job_id) +
                        ', marked as TERMINATE/DOWN (1).'
                    )
            # the container is not running, let's try to verify its historical state (error or not ?)
            elif str(worker.name) not in self.historical_containers:
                self.historical_containers.append(str(worker.name))
                try:
                    await self.job_queue_check_container_historical_state(worker)
                except JobardClusterCriticalError as je:
                    # in case of critical exception, we propagate it immediately, as a generic Jobard Error
                    raise JobardError(str(je))
                except JobardTimeoutError as je:
                    # in case of timeout, we'll retry later. we do not terminate the whole app.
                    self._logger.info(je)
                except JobardError as je:
                    # in case of non-critical error, we'll retry later. we do not terminate the whole app.
                    self._logger.info(je)
                # container marked as down
                containers_down += 1
                self._logger.info(
                    'Cluster monitoring, N:' + str(worker.name) + ' J:' + str(worker.job_id) +
                    ', marked as TERMINATE/DOWN (2).'
                )
            else:
                # container marked as down
                containers_down += 1
                self._logger.info(
                    'Cluster monitoring, N:' + str(worker.name) + ' J:' + str(worker.job_id) +
                    ', marked as TERMINATE/DOWN (3).'
                )

        self._logger.info(
            'Cluster monitoring, ' +
            str(expected_containers_count) + ' expected containers. ' +
            str(len(self.cluster.workers)) + ' dask registered containers. ' +
            str(containers_running) + ' running. ' +
            str(containers_in_queue) + ' in queue. ' +
            str(expected_containers_count - (containers_running + containers_in_queue)) + ' terminated (or down). ' +
            str(containers_down) + ' terminated (or down) (registered only). '
        )

        # if no containers are running
        if containers_running > 0:
            self._logger.info('Cluster monitoring, at least one worker (job) is running.')
        elif containers_in_queue > 0:
            self._logger.info(
                'Cluster monitoring, no containers are running. but at least one container is in queue. waiting.'
            )
        else:
            raise JobardError('Cluster monitoring, no containers are running. no containers are in queue. aborting.')

        # return the containers in RUNNING state count
        return containers_running

    def common_scheduler_health_check(self, scheduler):
        """Scheduler health check Routine."""

        # if we know, the client has already a task done, we skip the "one_container_up_time" test
        if self.one_task_done is False:
            if self.one_container_up_time is None:
                self._logger.info('Cluster monitoring, no container up. we skip the scheduler health check..')
                return
            # before checking the scheduler health, we need to be absolutely
            # sure that a container (a thing that contains a dask-worker process) is up.
            # that's why we rely on the dask-worker death_timeout
            current_time = int(time())
            if self.one_container_up_time > current_time - self.app.death_timeout:
                self._logger.info(
                    'Cluster monitoring, the first container has begun within the death_timeout delta.'
                    ' so we wait a bit before checking the health of the dask-workers'
                )
                return

        # the client starts when the cluster is ready, so the client must be alive at this point
        if len(scheduler.clients) == 0:
            raise JobardError('Cluster monitoring, no client is connected to the cluster')

        # check dask-workers states reported by the Dask scheduler
        if len(scheduler.workers) == 0:
            raise JobardError(
                'Cluster monitoring, no workers are connected to the cluster. it is a critical error at this point'
            )

        at_least_one_recently_seen = False
        for i in scheduler.workers:
            worker = scheduler.workers[i]

            current_time = int(time())
            last_seen = int(worker.last_seen)
            dt_object = datetime.fromtimestamp(last_seen)

            self._logger.info(
                'Cluster monitoring, DASK-WORKER STATUS (BINARY) - N:' +
                str(worker.name) + ' - A:'
                + worker.address + ' - P:'
                + str(len(worker.processing)) + ' - LS:'
                + dt_object.isoformat()
            )

            if last_seen > current_time - self.app.death_timeout:
                at_least_one_recently_seen = True

        if at_least_one_recently_seen is False:
            raise JobardError('Cluster monitoring, no worker recently seen')

    def job_queue_opt_routine(self, arguments):
        """Job Queue Common Options Routine."""

        # pass Dask scheduler/driver options
        if self.app.driver_extra_args is not None:
            if arguments.get('scheduler_options') is None:
                arguments['scheduler_options'] = self.app.driver_extra_args
            else:
                arguments.get('scheduler_options').extend(self.app.driver_extra_args)

        # lifetime for graceful shutdown worker against scheduler wall time
        if arguments.get('extra') is None:
            arguments['extra'] = self.lifetime()
        else:
            arguments.get('extra').extend(self.lifetime())

        # pass Dask workers options
        if self.app.worker_extra_args is not None:
            arguments.get('extra').extend(self.app.worker_extra_args)

    def job_queue_log_routine(self):
        """Job Queue Log Routine."""

        # log some common cluster info
        self._logger.info('Cluster, job script :' + '\n' + self.cluster.job_script())
        self._logger.info('Cluster, Dask Scheduler GUI : ' + self.cluster.dashboard_link)

    async def job_queue_scale_routine(self):
        """Job Queue Scale Routine."""

        # scale
        if self.app.n_workers is not None:
            await self.cluster.scale(n=self.app.n_workers)
        else:
            self._logger.warning('Cluster, adaptative cluster, is hard to get right.')
            self._logger.warning('Cluster, please use static scaling whenever possible.')
            await self.cluster.adapt(minimum=self.app.n_min_workers, maximum=self.app.n_max_workers)

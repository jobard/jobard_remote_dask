import logging
import pathlib
import random
import sys
from typing import List

from jobard_remote_dask.cli.app import run_app
from jobard_remote_dask.core.type.cluster import Cluster
from jobard_remote_dask.model.event.app import AppEvent
from jobard_remote_dask.model.event.job import JobEvent
from jobard_remote_dask.tcp.fakeclient import FakeTCPClient

if __name__ == '__main__':

    # app spec
    app = AppEvent(
        job_order_id=random.randint(1, sys.maxsize),
        job_array_id=1,
        try_id=1,
        command_alias='command',
        name='test_k8s',
        cluster_type=Cluster.KUBE,

        driver_cores=1,
        driver_memory='2GB',

        worker_cores=0.5,
        worker_memory='500MB',
        n_workers=2,

        log_chroot=pathlib.Path(r'/mnt/k8s/data/'),
        image='gitlab-registry.ifremer.fr/jobard/jobard_worker_dask:latest',
        docker_mount_points={'worker-data': ('/mnt/k8s/data', '/mnt/k8s/data')},
        application_processor_timeout=240,
        system_limit_timeout=120,
        death_timeout=60,
    )

    # job spec
    jobs: List[JobEvent] = []
    # job spec
    for index in range(100):
        rand_num = random.randint(10, 20)
        jobs.append(
            JobEvent(
                id=index,
                command=[
                    '/bin/sleep',
                    f'{rand_num}'
                ]
            )
        )

    # create the tcp client
    tcp_client = FakeTCPClient(
        simulate_ping_exception=False,
        simulate_cancellation_asked=False,
        app=app,
        jobs=jobs,
    )

    # run the app
    run_app(logging.DEBUG, tcp_client)

    sys.exit(0)

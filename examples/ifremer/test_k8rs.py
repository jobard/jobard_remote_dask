# Test with kr8s API for creating Service and Pods and retrieving resources info
from time import sleep

import kr8s
from kr8s.objects import Service, Namespace, Deployment


def print_pod_list():

    dep = Deployment.get(name='jobard-deployment', namespace='jobard')

    print('----------------------------------------------------------------------------------------')
    print("PODs LIST")
    print('{:>12} {:>55} {:>12} {:>15} {:>15}'.format('NAMESPACE', 'NAME', 'PHASE', 'POD IP', 'HOST IP'))
    for pod in dep.pods():
        pod.refresh()
        print('{:>12} {:>55} {:>12} {:>15} {:>15}'.format(
            pod.namespace, pod.name, pod.status.phase, pod.status.podIP, pod.status.hostIP))
    print('----------------------------------------------------------------------------------------')


def print_service_list():

    service = Service.get(name='jobard-service', namespace="jobard")

    print('----------------------------------------------------------------------------------------')
    print("SERVICEs")
    print('{:>12} {:>25} {:>12} {:>15} {:>15}'.format('NAMESPACE', 'NAME', 'TYPE', 'TARGET PORT', 'NODE PORT'))
    print('{:>12} {:>25} {:>12} {:>15} {:>15}'.format(service.metadata.namespace, service.metadata.name,
                                                      service.spec.type, service.spec.ports[0].targetPort,
                                                      service.spec.ports[0].nodePort))
    print('----------------------------------------------------------------------------------------')


def delete_deployment():

    # Delete the namespace => delete all resources belonging to the namespace (service, pods...)
    ns = Namespace.get(name='jobard')
    ns.delete()


def create_deployment():

    # Create the namespace
    ns = Namespace({
                            'apiVersion': 'v1',
                            'kind': 'Namespace',
                            'metadata': {
                                'name': 'jobard'
                            }
                        })
    ns.create()

    # Create the service
    service = Service({
                            'apiVersion': 'v1',
                            'kind': 'Service',
                            'metadata': {
                                'name': 'jobard-service',
                                'namespace': 'jobard'
                            },
                            'spec': {
                                'type': 'NodePort',
                                'selector': {
                                    'app': 'jobard-app'
                                },
                                'ports': [
                                    {
                                        'port': 5000,
                                        'targetPort': 5000,
                                        'name': 'jobard-worker-np'
                                    }
                                    ]
                                }
                        })
    service.create()
    sleep(2)

    # Create the deployment
    deploy = Deployment({
                'apiVersion': 'apps/v1',
                'kind': 'Deployment',
                'metadata': {
                    'name': 'jobard-deployment',
                    'namespace': 'jobard'
                },
                'spec': {
                    'replicas': 2,
                    'selector': {
                        'matchLabels': {
                            'app': 'jobard-app'
                        }
                    },
                    'template': {
                        'metadata': {
                            'labels': {
                                'app': 'jobard-app'
                            }
                        },
                        'spec': {
                            'containers': [
                                {
                                    'name': 'jobard-app',
                                    'image': 'hajdaini/flask:random',
                                    'imagePullPolicy': 'IfNotPresent'
                                }
                            ]
                        }
                    }
                }
    })
    deploy.create()
    sleep(2)
    # Check deployment
    for pod in deploy.pods():
        pod.refresh()
        print(f'POD : {pod.name} EXISTS : {pod.exists()} READY : {pod.ready()}')


if __name__ == '__main__':
    api = kr8s.api()
    # Create deployment
    create_deployment()

    # Print deployment infos
    print_service_list()
    print_pod_list()

    # Delete deployement resources
    delete_deployment()




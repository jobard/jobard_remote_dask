# Test with kr8s API for creating Service and Pods and retrieving resources info
from time import sleep
from dask.distributed import Client
import kr8s
from kr8s.objects import Service, Namespace, Deployment


def inc(x):
    return x + 1


def process_task(host_ip, host_port):
    dask_client = Client(host_ip+':'+str(host_port))

    # Run the computation at the cluster
    x = dask_client.submit(inc, 10)
    print(x.result())

def print_pod_list(dep_name):

    dep = Deployment.get(name=dep_name, namespace='jobard')

    print('----------------------------------------------------------------------------------------')
    print("PODs LIST")
    print('{:>12} {:>55} {:>12} {:>15} {:>15}'.format('NAMESPACE', 'NAME', 'PHASE', 'POD IP', 'HOST IP'))
    for pod in dep.pods():
        pod.refresh()
        print('{:>12} {:>55} {:>12} {:>15} {:>15}'.format(
            pod.namespace, pod.name, pod.status.phase, pod.status.podIP, pod.status.hostIP))
    print('----------------------------------------------------------------------------------------')


def print_service_list():

    service = Service.get(name='jobard-service', namespace="jobard")

    print('----------------------------------------------------------------------------------------')
    print("SERVICEs")
    print('{:>12} {:>25} {:>12} {:>15} {:>15}'.format('NAMESPACE', 'NAME', 'TYPE', 'TARGET PORT', 'NODE PORT'))
    print('{:>12} {:>25} {:>12} {:>15} {:>15}'.format(service.metadata.namespace, service.metadata.name,
                                                      service.spec.type, service.spec.ports[0].targetPort,
                                                      service.spec.ports[0].nodePort))
    print('----------------------------------------------------------------------------------------')


def delete_deployment():

    # Delete the namespace => delete all resources belonging to the namespace (service, pods...)
    ns = Namespace.get(name='jobard')
    ns.delete()


def create_deployment():

    # Create the namespace
    ns = Namespace({
                            'apiVersion': 'v1',
                            'kind': 'Namespace',
                            'metadata': {
                                'name': 'jobard'
                            }
                        })
    ns.create()

    # Create the service
    service = Service({
                            'apiVersion': 'v1',
                            'kind': 'Service',
                            'metadata': {
                                'name': 'jobard-service',
                                'namespace': 'jobard'
                            },
                            'spec': {
                                'type': 'NodePort',
                                'selector': {
                                    'app': 'jobard-app'
                                },
                                'ports': [
                                    {
                                        'port': 9000,
                                        'targetPort': 9000,
                                        'name': 'jobard-worker-np'
                                    },
                                    # TODO CR For dask worker without jobard preload
                                    {
                                        'port': 8786,
                                        'targetPort': 8786,
                                        'name': 'scheduler-np'
                                    }
                                    ]
                                }
                        })
    service.create()
    sleep(2)

    # Get Host port
    service = Service.get(name='jobard-service', namespace="jobard")
    sc_host_port = next(
        (item.nodePort for item in service.spec.ports if item.name == 'scheduler-np'),
        None
    )

    # Create the deployment for the scheduler and the workers
    sc_deploy = Deployment({
        'apiVersion': 'apps/v1',
        'kind': 'Deployment',
        'metadata': {
            'name': 'jobard-deployment-scheduler',
            'namespace': 'jobard'
        },
        'spec': {
            'replicas': 1,
            'selector': {
                'matchLabels': {
                    'app': 'jobard-app',
                    'role': 'scheduler'
                }
            },
            'template': {
                'metadata': {
                    'labels': {
                        'app': 'jobard-app',
                        'role': 'scheduler'
                    }
                },
                'spec': {
                    'containers': [
                        {
                            'name': 'jobard-scheduler',
                            'image': 'gitlab-registry.ifremer.fr/jobard/jobard_worker_dask:latest',
                            'imagePullPolicy': 'IfNotPresent',
                            'command': ['dask-scheduler'],
                        }
                    ]
                }
            }
        }
    })
    sc_deploy.create()
    while not sc_deploy.ready():
        sleep(2)
    # Check deployment
    sc_host_ip = None
    sc_pod_ip = None
    sc_port = str(8786) # TODO CR port scheduler-np du service

    for pod in sc_deploy.pods():
        pod.refresh()
        # Get the host IP of the scheduler
        sc_host_ip = pod.status.hostIP
        sc_pod_ip = str(pod.status.podIP)
        print(f'POD : {pod.name} EXISTS : {pod.exists()} READY : {pod.ready()}')

    wk_deploy = Deployment({
                'apiVersion': 'apps/v1',
                'kind': 'Deployment',
                'metadata': {
                    'name': 'jobard-deployment-worker',
                    'namespace': 'jobard'
                },
                'spec': {
                    'replicas': 2,
                    'selector': {
                        'matchLabels': {
                            'app': 'jobard-app',
                            'role': 'worker'
                        }
                    },
                    'template': {
                        'metadata': {
                            'labels': {
                                'app': 'jobard-app',
                                'role': 'worker'
                            }
                        },
                        'spec': {
                            'containers': [
                                {
                                    'name': 'jobard-worker',
                                    'image': 'gitlab-registry.ifremer.fr/jobard/jobard_worker_dask:latest',
                                    'imagePullPolicy': 'IfNotPresent',
                                    # adresse IP du pod du sheduler + port du service
                                    # dask-worker 10.0.0.87:8786
                                    # WORKER ARGS : /venv/jobard_worker_dask/bin/dask-worker tcp://10.81.60.1:8786 --local-directory /tmp/ --nthreads 1 --memory-limit auto --listen-address tcp://0.0.0.0:9000 --no-nanny --no-dashboard --death-timeout 60 --resources CPU=1,MEM=2000000000
                                    'command': [
                                        '/venv/jobard_worker_dask/bin/dask-worker',
                                        sc_pod_ip+':'+sc_port,
                                        '--local-directory', '/tmp/','--nthreads', '1', '--memory-limit', 'auto',
                                        #'--listen-address', 'tcp://0.0.0.0:9000',
                                        '--no-nanny', '--no-dashboard', '--death-timeout', '60',
                                        '--resources', 'CPU=1,MEM=2000000000',
                                        #'--preload', '/venv/jobard_worker_dask/lib/python3.9/site-packages/jobard_worker_dask/swarm.py',
                                        #'--mapping-tcp-host', sc_pod_ip,
                                        #'--mapping-tcp-port', sc_port,
                                    ]
                                }
                            ]
                        }
                    }
                }
    })
    wk_deploy.create()
    while not wk_deploy.ready():
        sleep(2)
    # Check deployment
    for pod in wk_deploy.pods():
        pod.refresh()
        print(f'POD : {pod.name} EXISTS : {pod.exists()} READY : {pod.ready()}')

    return sc_host_ip, sc_host_port


if __name__ == '__main__':
    api = kr8s.api()
    # Create deployment
    sc_host_ip, sc_host_port = create_deployment()

    # Print deployment infos
    print_service_list()
    print_pod_list('jobard-deployment-worker')
    print_pod_list('jobard-deployment-scheduler')

    # Compute a task
    process_task(sc_host_ip, sc_host_port)

    # Delete deployement resources
    delete_deployment()




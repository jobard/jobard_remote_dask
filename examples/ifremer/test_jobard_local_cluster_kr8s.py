# Test with kr8s API for creating Service and Pods and retrieving resources info
import asyncio
import fcntl
import pathlib
import struct
from time import sleep
import socket

from dask import distributed
from dask.distributed import Client
import kr8s
from kr8s.objects import Service, Namespace, Deployment

# TCP Mapping Server global timeout
TCP_MAPPING_SERVER_GLOBAL_TIMEOUT: int = 40

def inc(x):
    return x + 1


def process_task(local_cluster_adr):

    dask_client = Client(address=local_cluster_adr)

    print('-------------- avant submit------------------')
    dask_client.get_scheduler_logs()
    #dask_client.get_worker_logs()

    # Run the computation at the cluster
    x = dask_client.submit(inc, 10)

    print('-------------- apres submit------------------')
    dask_client.get_scheduler_logs()
    #dask_client.get_worker_logs()

    print(x.result())

    print('-------------- apres result ------------------')
    dask_client.get_scheduler_logs()
    #dask_client.get_worker_logs()

def print_pod_list(dep_name):

    dep = Deployment.get(name=dep_name, namespace='jobard')

    print('----------------------------------------------------------------------------------------')
    print("PODs LIST")
    print('{:>12} {:>55} {:>12} {:>15} {:>15}'.format('NAMESPACE', 'NAME', 'PHASE', 'POD IP', 'HOST IP'))
    for pod in dep.pods():
        pod.refresh()
        print('{:>12} {:>55} {:>12} {:>15} {:>15}'.format(
            pod.namespace, pod.name, pod.status.phase, pod.status.podIP, pod.status.hostIP))
    print('----------------------------------------------------------------------------------------')


def print_service_list():

    service = Service.get(name='jobard-service', namespace="jobard")

    print('----------------------------------------------------------------------------------------')
    print("SERVICEs")
    print('{:>12} {:>25} {:>12} {:>15} {:>15}'.format('NAMESPACE', 'NAME', 'TYPE', 'TARGET PORT', 'NODE PORT'))
    print('{:>12} {:>25} {:>12} {:>15} {:>15}'.format(service.metadata.namespace, service.metadata.name,
                                                      service.spec.type, service.spec.ports[0].targetPort,
                                                      service.spec.ports[0].nodePort))
    print('----------------------------------------------------------------------------------------')


def delete_deployment():

    # Delete the namespace => delete all resources belonging to the namespace (service, pods...)
    ns = Namespace.get(name='jobard')
    ns.delete()


def create_local_cluster():

    # Init the Dask LocalCluster
    # build cluster arguments
    arguments = {

        # log directory for both workers output and jobs output
        'log_directory': '/tmp' + pathlib.os.sep,

        # async usage
        # TODO CR
        #'asynchronous': True,

        # always use processes
        'processes': True,

        # workers will be added later
        'n_workers': 0,

        # insecure protocol supported only
        'protocol': 'tcp://',

        # set network interface
        # TODO CR
        # 'interface': dask.config.get('swarm.public_interface'),
        # 'interface': 'lo', # ens3 n'est pas accessible depuis localhost
        'interface': 'mpqemubr0'
    }

    # create the cluster
    local_cluster = distributed.LocalCluster(**arguments)

    # log some cluster info
    print(f'LOCAL CLUSTER ADDRESS : {local_cluster.scheduler.address}')

    # get the scheduler address
    return local_cluster.scheduler.address


def create_deployment(sc_pod_ip, sc_port):

    # Create the namespace
    ns = Namespace({
                            'apiVersion': 'v1',
                            'kind': 'Namespace',
                            'metadata': {
                                'name': 'jobard'
                            }
                        })
    ns.create()

    # Create the service
    service = Service({
                            'apiVersion': 'v1',
                            'kind': 'Service',
                            'metadata': {
                                'name': 'jobard-service',
                                'namespace': 'jobard'
                            },
                            'spec': {
                                'type': 'NodePort',
                                'selector': {
                                    'app': 'jobard-app'
                                },
                                'ports': [
                                    {
                                        'port': 9000,
                                        'targetPort': 9000,
                                        'name': 'jobard-worker-np'
                                    },
                                    # TODO CR For dask worker without jobard preload
                                    {
                                        'port': 8786,
                                        'targetPort': 8786,
                                        'name': 'scheduler-np'
                                    }
                                    ]
                                }
                        })
    service.create()
    sleep(2)

    # Get Host port
    service = Service.get(name='jobard-service', namespace="jobard")
    sc_host_port = next(
        (item.nodePort for item in service.spec.ports if item.name == 'scheduler-np'),
        None
    )

    # Create the deployment for the scheduler and the workers
    wk_deploy = Deployment({
                'apiVersion': 'apps/v1',
                'kind': 'Deployment',
                'metadata': {
                    'name': 'jobard-deployment-worker',
                    'namespace': 'jobard'
                },
                'spec': {
                    'replicas': 1,
                    'selector': {
                        'matchLabels': {
                            'app': 'jobard-app',
                            'role': 'worker'
                        }
                    },
                    'template': {
                        'metadata': {
                            'labels': {
                                'app': 'jobard-app',
                                'role': 'worker'
                            }
                        },
                        'spec': {
                            'containers': [
                                {
                                    'name': 'jobard-worker',
                                    'image': 'gitlab-registry.ifremer.fr/jobard/jobard_worker_dask:latest',
                                    'imagePullPolicy': 'IfNotPresent',
                                    # adresse IP du pod du sheduler + port du service
                                    # dask-worker 10.0.0.87:8786
                                    # WORKER ARGS : /venv/jobard_worker_dask/bin/dask-worker tcp://10.81.60.1:8786 --local-directory /tmp/ --nthreads 1 --memory-limit auto --listen-address tcp://0.0.0.0:9000 --no-nanny --no-dashboard --death-timeout 60 --resources CPU=1,MEM=2000000000
                                    'command': [
                                        '/venv/jobard_worker_dask/bin/dask-worker',
                                        sc_pod_ip+':'+sc_port,
                                        '--local-directory', '/tmp/','--nthreads', '1', '--memory-limit', 'auto',
                                        '--listen-address', 'tcp://0.0.0.0:9000',
                                        '--no-nanny', '--no-dashboard', '--death-timeout', '60',
                                        '--resources', 'CPU=1,MEM=2000000000',
                                    ]
                                }
                            ]
                        }
                    }
                }
    })
    wk_deploy.create()
    while not wk_deploy.ready():
        sleep(2)
    # Check deployment
    for pod in wk_deploy.pods():
        pod.refresh()
        print(f'POD : {pod.name} EXISTS : {pod.exists()} READY : {pod.ready()}')

    return sc_host_port


if __name__ == '__main__':

    # Create the Dask local cluster
    scheduler_adr = create_local_cluster()
    #'tcp://10.81.60.1:33191'
    scheduler_adr.split(':')
    tmp = scheduler_adr.split(':')[1]
    sc_pod_ip = tmp[-10:]
    sc_port = scheduler_adr.split(':')[2]
    # Create deployment
    api = kr8s.api()
    sc_host_port = create_deployment(sc_pod_ip,sc_port)

    # Print deployment infos
    print_service_list()
    print_pod_list('jobard-deployment-worker')

    # Compute a task
    process_task(scheduler_adr)

    # Delete deployement resources
    delete_deployment()




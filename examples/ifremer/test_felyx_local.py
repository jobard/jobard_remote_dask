import logging
import os
import pathlib
import sys
from typing import List

from jobard_remote_dask.cli.app import run_app
from jobard_remote_dask.core.type.app import AppType
from jobard_remote_dask.core.type.cluster import Cluster
from jobard_remote_dask.model.event.app import AppEvent
from jobard_remote_dask.model.event.job import JobEvent
from jobard_remote_dask.tcp.fakeclient import FakeTCPClient

if __name__ == '__main__':

    # command
    command = [
        '/opt/miniconda3/envs/felyx_processor_cerbere/bin/felyx-extraction',
        '-c',
        '/home/criou/PycharmProjects/felyx_processor/tests/resources/test_slstr_a_cmems/s3a_mmdb_local.yaml',
        '--dataset-id',
        'S3A_SL_2_WST__OPE_NRT',
        '--child-product-dir',
        '/data/felyx/tests/test_jobard/child_products/',
        '--create-miniprod',
        '--manifest-dir',
        '/data/felyx/tests/test_jobard/manifests/']

    # app spec
    app = AppEvent(
        job_order_id=3,
        job_array_id=2,
        try_id=3,
        command_alias=os.path.basename(command[0]),
        job_order_name=command[4],
        name='plop',
        app_type=AppType.DASK_JOB_ARRAY,
        cluster_type=Cluster.LOCAL,

        walltime='00:10:00',

        driver_cores=1,
        driver_memory='2GB',

        worker_cores=1,
        worker_memory='2GB',
        n_workers=2,
        n_min_workers=None,
        n_max_workers=None,

        log_chroot=pathlib.Path(r'/data/jobard/log/'),

        application_processor_timeout=240,
        system_limit_timeout=120,
        death_timeout=60,
    )
    # Felyx extraction
    # job spec
    in_files = [
        '/home/criou/PycharmProjects/felyx_processor/tests/data/eo/slstr-a/wst_granule/S3A_SL_2_WST____20210915T043506_20210915T043806_20210915T063954_0179_076_204_3420_MAR_O_NR_003.SEN3',
        '/home/criou/PycharmProjects/felyx_processor/tests/data/eo/slstr-a/wst_granule/S3A_SL_2_WST____20210915T065505_20210915T065805_20210915T082720_0179_076_205_5760_MAR_O_NR_003.SEN3',
        '/home/criou/PycharmProjects/felyx_processor/tests/data/eo/slstr-a/wst_granule/S3A_SL_2_WST____20210915T220858_20210915T221158_20210915T232209_0179_076_215_0000_MAR_O_NR_003.SEN3'
    ]

    jobs: List[JobEvent] = []
    for index, file in enumerate(in_files):
        jobs.append(
            JobEvent(
                id=index+1,
                #job_prefix=os.path.basename(file),
                command= command + ['--inputs', file]
            )
        )

    # create the tcp client
    tcp_client = FakeTCPClient(
        simulate_ping_exception=False,
        simulate_cancellation_asked=False,
        app=app,
        jobs=jobs,
    )

    # run the app
    run_app(logging.INFO, tcp_client)

    sys.exit(0)

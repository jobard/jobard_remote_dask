import logging
import os
import pathlib
import random
import sys
from typing import List

import pandas as pd

from jobard_remote_dask.cli.app import run_app
from jobard_remote_dask.core.type.app import AppType
from jobard_remote_dask.core.type.cluster import Cluster
from jobard_remote_dask.model.event.app import AppEvent
from jobard_remote_dask.model.event.job import JobEvent
from jobard_remote_dask.tcp.fakeclient import FakeTCPClient

if __name__ == '__main__':

    # app spec
    app = AppEvent(
        job_order_id=random.randint(1, sys.maxsize),
        job_array_id=1,
        try_id=1,
        command_alias='command',
        name='plop',
        app_type=AppType.DASK_JOB_ARRAY,
        cluster_type=Cluster.HTCONDOR,

        driver_cores=1,
        driver_memory='2GB',

        worker_cores=1,
        worker_memory='1GB',
        n_workers=2,
        n_min_workers=None,
        n_max_workers=None,

        log_chroot=pathlib.Path(r'/home1/datahome/cerint/jobard/log/'),

        walltime='00:30:00',

        application_processor_timeout=240,
        system_limit_timeout=120,
        death_timeout=600,

        driver_extra_args=None,
        worker_extra_args=None,
        jobard_extra_args=None,
    )

    # job spec
    df = pd.read_csv(os.path.dirname(__file__) + '/jobard_remote_dask/tests/files.csv', header=None, nrows=100)
    df = df.reset_index()
    jobs: List[JobEvent] = []
    for index, row in df.iterrows():
        file = row[0]
        jobs.append(
            JobEvent(
                id=index,
                command=[
                    '/home1/datahome/cerint/.conda/envs/felyx_processor_2.1.1/bin/felyx-extraction',
                    '-c',
                    '/home1/datahome/cerint/candre/felyx_data_maxss.yaml',
                    '--dataset_id',
                    'SEALEVEL_GLO_PHY_L3_REP_OBSERVATIONS_008_062_S3A',
                    '--miniprod_dir',
                    '/home1/datahome/cerint/candre/data/',
                    '--manifest_dir',
                    '/home1/datahome/cerint/candre/manifests/',
                    '--inputs',
                    file,
                ]
            )
        )

    # create the tcp client
    tcp_client = FakeTCPClient(
        simulate_ping_exception=False,
        simulate_cancellation_asked=False,
        app=app,
        jobs=jobs,
    )

    # run the app
    run_app(logging.INFO, tcp_client)

    sys.exit(0)

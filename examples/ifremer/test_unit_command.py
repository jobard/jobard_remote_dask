import os
import resource
import signal
import subprocess
from typing import List

import pandas as pd

from jobard_remote_dask.core.helpers.human import parse_human_size


def exec_custom_binary(
        command: List[str],
        timeout_seconds: int,
        memory_bytes: int,
):
    is_success = False

    def pre_exec_fn():
        os.setsid()
        if timeout_seconds > 0:
            resource.setrlimit(resource.RLIMIT_CPU, (timeout_seconds, timeout_seconds))
        resource.setrlimit(resource.RLIMIT_AS, (memory_bytes, memory_bytes))

    with subprocess.Popen(
            command,
            preexec_fn=pre_exec_fn,
            shell=False
    ) as p:
        try:
            ret = p.wait(timeout=(None if timeout_seconds < 0 else timeout_seconds))  # per unit job timeout
            is_success = True

        # except subprocess.TimeoutExpired:
        finally:
            # try to terminate at once the process (and possibly all of its children)
            try:
                # we terminate here the whole session (PID == SID) ...
                os.killpg(os.getpgid(p.pid), signal.SIGTERM)
                p.terminate()
            except Exception as e:
                print(str(e))
    print(str(ret))

    return is_success


def example1():
    exec_custom_binary(
        ['/home1/datahome/cerint/.conda/envs/felyx_processor/bin/python', '-c', 'print("hello world");'],
        300,
        parse_human_size('2GB')
    )


def example2():
    file = None
    df = pd.read_csv(os.path.dirname(__file__) + '/jobard_remote_dask/tests/files.csv', header=None, nrows=1)
    df = df.reset_index()
    for index, row in df.iterrows():
        file = row[0]
    exec_custom_binary(
        [
            '/home1/datahome/cerint/.conda/envs/felyx_processor/bin/python',
            '/home1/datahome/cerint/.conda/envs/felyx_processor/bin/felyx-extraction',
            '-c',
            '/home1/datahome/cerint/candre/felyx_data_maxss.yaml',
            '--dataset_id',
            'SEALEVEL_GLO_PHY_L3_REP_OBSERVATIONS_008_062_S3A',
            '--miniprod_dir',
            '/home1/datawork/cerint/candre/data/',
            '--manifest_dir',
            '/home1/datawork/cerint/candre/manifests/',
            '--inputs',
            file,
        ],
        300,
        parse_human_size('2GB')
    )


if __name__ == '__main__':
    example2()

import logging
import pathlib
import random
import sys
from typing import List

import pandas as pd

from jobard_remote_dask.cli.app import run_app
from jobard_remote_dask.core.type.app import AppType
from jobard_remote_dask.core.type.cluster import Cluster
from jobard_remote_dask.model.event.app import AppEvent
from jobard_remote_dask.model.event.job import JobEvent
from jobard_remote_dask.tcp.fakeclient import FakeTCPClient

"""
run with :
$ ssh cerint@datarmor
$ /home1/datahome/cerint/.conda/envs/jobard_remote_dask/bin/python /
    /home1/datahome/cerint/candre/jobard_remote_dask/test_pbs_jeff.py
"""

if __name__ == '__main__':

    # app spec
    app = AppEvent(

        # the logs will be in /home1/datahome/cerint/jobard/log/job_order={RANDOM_JOB_ORDER_ID_HERE}/job_array=1/try=1/
        # - the cluster sub-directory contains the logs from PBS
        # - the jobs sub-directory contains the felyx logs
        job_order_id=random.randint(1, sys.maxsize),
        log_chroot=pathlib.Path(r'/home1/datahome/cerint/jobard/log/'),

        # number of workers needed, and memory per worker
        worker_memory='2GB',
        n_workers=20,

        # remember the application total time is (a given worker wall time + application_processor_timeout seconds)
        walltime='04:00:00',  # per worker wall time
        application_processor_timeout=2000,  # we need to take into account the time when our workers might be in queue
        death_timeout=600,  # 600 seconds, because DATARMOR can be very slow times to times

        # ignore following conf properties for your tests
        queue='sequentiel',
        worker_cores=1,
        job_array_id=1,
        try_id=1,
        command_alias='command',
        name='jeff_app',
        app_type=AppType.DASK_JOB_ARRAY,
        cluster_type=Cluster.PBS,
        driver_cores=1,
        driver_memory='2GB',
    )

    # job spec
    df = pd.read_csv('/home1/datahome/jfpiolle/scripts/felyx/s3/avhrr.list', header=None)
    df = df.reset_index()
    jobs: List[JobEvent] = []
    for index, row in df.iterrows():
        file = row[0]
        jobs.append(
            JobEvent(
                id=index,
                command=[
                    '/home1/datahome/cerint/.conda/envs/felyx_processor_2.1.1/bin/felyx-extraction',
                    '-c',
                    '/home1/datahome/jfpiolle/git/felyx_eumetsat/conf/mmdb/template/metop-b_avhrr__cmems_mmdb.yaml',
                    '--dataset_id',
                    'AVHRR_SST_METOP_B-OSISAF-L2P-v1.0',
                    '--manifest_dir',
                    '/home1/datahome/cerint/jfpiolle/manifests/',
                    '--inputs',
                    file,
                ]
            )
        )

    # create the tcp client
    tcp_client = FakeTCPClient(
        simulate_ping_exception=False,
        simulate_cancellation_asked=False,
        app=app,
        jobs=jobs,
    )

    # run the app
    run_app(logging.INFO, tcp_client)

    sys.exit(0)

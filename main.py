"""Retro-compatiblity main, in order to support, not conda-ised case."""
import logging
import sys

from jobard_remote_dask.cli import defaults
from jobard_remote_dask.cli.app import run_app
from jobard_remote_dask.tcp.client import TCPClient

if __name__ == '__main__':

    # create the tcp client
    tcp_client = TCPClient(
        timeout=defaults.TCP_TIMEOUT,
        connect_timeout=defaults.TCP_CONNECT_TIMEOUT,
        host=sys.argv[1],
        port=int(sys.argv[2])
    )

    # run the app
    run_app(logging.INFO, tcp_client)

    sys.exit(0)

import pytest

from jobard_remote_dask.core.exceptions import JobardConversionError
from jobard_remote_dask.core.helpers.human import parse_human_size


@pytest.mark.parametrize('string, expected', [
    ('B', 1),
    ('', 1),
    ('1kb', 1000),
    ('1Kb', 1000),
    ('1KB', 1000),
    ('1KiB', 1024),
    ('1MB', 1000000),
    ('1MiB', 1048576),
    ('-1B', -1)
])
def test_parse_human_size(string, expected):
    assert parse_human_size(string) == expected


@pytest.mark.parametrize('string', [
    'fake',
    '1fake',
    'fake1B'
])
def test_parse_human_size_ko(string):
    with pytest.raises(JobardConversionError):
        parse_human_size(string)

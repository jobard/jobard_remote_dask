import pytest

from jobard_remote_dask.core.helpers.argument import (
    ARGUMENTS_UNABLE_TO_DETERMINE_DYNAMIC_ARGS,
    arguments_all,
    arguments_diff,
    arguments_extract,
)


@pytest.mark.parametrize('arr, expected', [
    (
        [
            ['extraction', 'plop', 'plip'],
            ['extraction', 'plap', 'plkp'],
            ['extraction', 'plap', 'plip'],
        ],
        [1, 2],
    ),
    (
        [
            ['extraction', 'plop', 'plip'],
            ['extraction', 'plap', 'plip'],
            ['extraction', 'plap', 'plip'],
        ],
        [1],
    ),
])
def test_arguments_diff(arr, expected):
    assert arguments_diff(arr) == expected


@pytest.mark.parametrize('arr, diff, expected', [
    (
        [
            ['extraction', 'plop', 'plip'],
            ['extraction', 'plap', 'plip'],
            ['extraction', 'plap', 'plip'],
        ],
        [1],
        'plop\nplap\nplap'
    ),
    (
        [
            ['extraction', 'plop', 'plip'],
            ['extraction', 'plap', 'plip'],
            ['extraction', 'plap', 'plip'],
        ],
        [1, 2],
        'plop,plip\nplap,plip\nplap,plip'
    ),
    (
        [
            ['extraction', 'plop', 'plip'],
            ['extraction', 'plap', 'plip'],
            ['extraction', 'plap', 'plip'],
        ],
        [],
        ARGUMENTS_UNABLE_TO_DETERMINE_DYNAMIC_ARGS +
        '\nextraction,plop,plip\nextraction,plap,plip\nextraction,plap,plip'
    ),
])
def test_arguments_extract(arr, diff, expected):
    assert arguments_extract(arr, diff) == expected


@pytest.mark.parametrize('arr, expected', [
    (
        [
            ['extraction', 'plop', 'plip'],
            ['extraction', 'plap', 'plip'],
            ['extraction', 'plap', 'plip'],
        ],
        [0, 1, 2]
    ),
])
def test_arguments_all(arr, expected):
    assert arguments_all(arr) == expected

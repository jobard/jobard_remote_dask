from jobard_remote_dask.core.helpers.net import get_free_port, get_hostname, get_ip_address


def test_get_ip_address():
    assert get_ip_address('lo') == '127.0.0.1'


def test_get_hostname():
    assert get_hostname() is not None


def test_get_free_port():
    assert get_free_port(get_hostname()) is not None
